/*
 * ObstAvoidSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef OBSTAVOIDSM_HPP_
#define OBSTAVOIDSM_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan{
	struct Replan;

	struct ObstAvoidSM:sc::state<ObstAvoidSM, MovingSM, Replan>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ReachDest>,
				sc::custom_reaction<Ev_ReplanPathFound>
				> reactions;
		typedef sc::state<ObstAvoidSM, MovingSM, Replan> Base;

		ObstAvoidSM(my_context ctx):Base(ctx){
			ROS_WARN("Enter Obstacle Avoid");
			static_pointer_cast<ObstAvoidGoal>(context<NaviSM>().obst_avoid_goal_)->makeSubGoal();
		}

		virtual ~ObstAvoidSM(){
			context<NaviSM>().dubins_planner_->resetPlannerStatus();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}

		sc::result react(const Ev_ReplanPathFound&){
			return transit<NormPlanSM>();
		}
	};

	struct Replan:sc::state<Replan, ObstAvoidSM>{
		typedef mpl::list<
				sc::custom_reaction<Ev_ReplanPathFound>,
				sc::custom_reaction<Ev_ObstAvoid>,
				sc::custom_reaction<Ev_ReachDest>
				> reactions;

		typedef sc::state<Replan, ObstAvoidSM> Base;

		Replan(my_context ctx):Base(ctx){
			ROS_WARN("Enter Replan");
			context<NaviSM>().dubins_planner_->updateGoal(static_pointer_cast<ObstAvoidGoal>(context<NaviSM>().obst_avoid_goal_)->sub_goal_);
			context<NaviSM>().dubins_planner_->plan_timer_.start();
		}

		virtual ~Replan(){

		}

		sc::result react(const Ev_ReplanPathFound&){
			return forward_event();
		}

		sc::result react(const Ev_ObstAvoid&){
			return discard_event();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}
	};
}

#endif /* OBSTAVOIDSM_HPP_ */
