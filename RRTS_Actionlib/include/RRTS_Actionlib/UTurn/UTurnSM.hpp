/*
 * UTurnSM.hpp
 *
 *  Created on: 25 Aug, 2014
 *      Author: liuwlz
 */

#ifndef UTURNSM_HPP_
#define UTURNSM_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan {

	struct UTurnSM:sc::state<UTurnSM, MovingSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_UTurn>,
				sc::custom_reaction<Ev_UTurnPathFound>
		> reactions;
		typedef sc::state<UTurnSM, MovingSM> Base;

		UTurnSM(my_context ctx):Base(ctx){
			ROS_WARN("Enter UTurn");
			static_pointer_cast<UTurnGoal>(context<NaviSM>().uturn_goal_)->makeUTurnGoal();
			context<NaviSM>().reedshepp_planner_->updateGoal(static_pointer_cast<UTurnGoal>(context<NaviSM>().uturn_goal_)->uturn_goal_);
			context<NaviSM>().reedshepp_planner_->plan_timer_.start();
		}

		virtual ~UTurnSM(){
			context<NaviSM>().reedshepp_planner_->resetPlannerStatus();
		}

		sc::result react(const Ev_UTurnPathFound){
			return transit<NormPlanSM>();
		}

		sc::result react(const Ev_UTurn&){
			return discard_event();
		}
	};
}  // namespace BehaviorPlan

#endif /* UTURNSM_HPP_ */
