/*
 * ManualGoal.hpp
 *
 *  Created on: 21 Apr, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_RRTS_ACTIONLIB_MANUALGOAL_MANUALGOAL_HPP_
#define INCLUDE_RRTS_ACTIONLIB_MANUALGOAL_MANUALGOAL_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>


namespace BehaviorPlan {

	struct ManualGoalWaiting;
	struct ManualGoalPlanning;

	struct ManualGoalSM:sc::state<ManualGoalSM, MovingSM, ManualGoalWaiting>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ReachDest>,
				sc::custom_reaction<Ev_UTurnPathFound>
				> reactions;
		typedef sc::state<ManualGoalSM, MovingSM, ManualGoalWaiting> Base;

		ManualGoalSM(my_context ctx):Base(ctx){
			ROS_WARN("Enter ManualGoal");
		}

		virtual ~ManualGoalSM(){
			context<NaviSM>().reedshepp_planner_->resetPlannerStatus();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}

		sc::result react(const Ev_UTurnPathFound&){
			return transit<NormPlanSM>();
		}
	};

	struct ManualGoalWaiting:sc::state<ManualGoalWaiting, ManualGoalSM>{
		typedef mpl::list<
				sc::custom_reaction<Ev_ReachDest>,
				sc::custom_reaction<Ev_ManualGoalGot>
				> reactions;

		typedef sc::state<ManualGoalWaiting, ManualGoalSM> Base;

		ManualGoalWaiting(my_context ctx):Base(ctx){
			ROS_WARN("Enter ManualGoalWaiting!");
			context<NaviSM>().reedshepp_planner_->plan_timer_.stop();
			context<NaviSM>().reedshepp_planner_->resetPlannerStatus();
		}

		virtual ~ManualGoalWaiting(){
			ROS_INFO("Got Manual Goal!");
		}

		sc::result react(const Ev_ManualGoalGot&){
			return transit<ManualGoalPlanning>();
		}

		sc::result react(const Ev_ReachDest&){
			return forward_event();
		}
	};

	struct ManualGoalPlanning:sc::state<ManualGoalPlanning, ManualGoalSM>{
		typedef mpl::list<
				sc::custom_reaction<Ev_UTurnPathFound>,
				sc::custom_reaction<Ev_ManualGoal>,
				sc::custom_reaction<Ev_ReachDest>
			> reactions;

		typedef sc::state<ManualGoalPlanning, ManualGoalSM> Base;

		ManualGoalPlanning(my_context ctx):Base(ctx){
			ROS_WARN("Enter ManualGoalPlanning");
			context<NaviSM>().reedshepp_planner_->updateGoal(context<NaviSM>().mannual_goal_);
			context<NaviSM>().reedshepp_planner_->plan_timer_.start();
			ros::spin();
		}

		virtual ~ManualGoalPlanning(){

		}

		sc::result react (const Ev_UTurnPathFound&){
			return transit<ManualGoalWaiting>();
		}

		sc::result react(const Ev_ManualGoal&){
			return discard_event();
		}

		sc::result react(const Ev_ReachDest&){
			return forward_event();
		}

	};

}  // namespace BehaviorPlan


#endif /* INCLUDE_RRTS_ACTIONLIB_MANUALGOAL_MANUALGOAL_HPP_ */
