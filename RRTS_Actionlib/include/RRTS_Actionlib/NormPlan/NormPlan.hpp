/*
 * NormPlan.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NORMPLAN_HPP_
#define NORMPLAN_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan{

	struct NormPlanSM : sc::state<NormPlanSM,MovingSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ObstAvoid>,
				sc::custom_reaction<Ev_UTurn>,
				sc::custom_reaction<Ev_ManualGoal>,
				sc::custom_reaction<Ev_ReachDest>
				> reactions;
		typedef sc::state<NormPlanSM, MovingSM> Base;
		NormPlanSM(my_context ctx) : Base(ctx){
			ROS_WARN("Enter Normal Plan");
			context<NaviSM>().vehicle_ready_ = true;
			context<NaviSM>().dubins_planner_->plan_timer_.stop();
			context<NaviSM>().reedshepp_planner_->plan_timer_.stop();
		}

		virtual ~NormPlanSM(){

		}

		sc::result react(const Ev_ObstAvoid &){
			return transit<ObstAvoidSM>();
		}

		sc::result react(const Ev_UTurn &){
			return transit<UTurnSM>();
		}

		sc::result react(const Ev_ManualGoal&){
			return transit<ManualGoalSM>();
		}

		sc::result react(const Ev_ReachDest& ){
			return forward_event();
		}
	};
}

#endif /* NORMPLAN_HPP_ */
