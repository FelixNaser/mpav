/*
 * NavigationSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef NAVIGATIONSM_HPP_
#define NAVIGATIONSM_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

using namespace MotionPlan;
using namespace Perception;
using namespace Util;

namespace BehaviorPlan{

	struct NaviSM:sc::state_machine<NaviSM, MissionWaitingSM>{

		NaviSM(){
			ROS_WARN("Welcome Aboard!");
		};
		virtual ~NaviSM(){};

//		shared_ptr<ExtendMetricMap> extend_metric_map_;
//		shared_ptr<PriorMetricMap> prior_metric_map_;

		shared_ptr<MetricMap> metric_map_;

		shared_ptr<RRTSPlanner> dubins_planner_, reedshepp_planner_;
		shared_ptr<GoalGenerator> obst_avoid_goal_, uturn_goal_;

		geometry_msgs::PoseStamped mannual_goal_;
		nav_msgs::Path reference_path_;
		bool vehicle_ready_;
		int metric_map_type_, planner_type_;
	};
}

#endif /* NAVIGATIONSM_HPP_ */
