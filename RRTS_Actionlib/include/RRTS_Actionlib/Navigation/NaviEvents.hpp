/*
 * NaviEvents.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NAVIEVENTS_HPP_
#define NAVIEVENTS_HPP_

#include <boost/statechart/event.hpp>

namespace sc = boost::statechart;

namespace BehaviorPlan{

	/**
	 *	Navi level events
	 */
	struct Ev_MissionRecived:sc::event<Ev_MissionRecived>{

	};

	struct Ev_PerceptionInitilized:sc::event<Ev_PerceptionInitilized>{

	};

	struct Ev_ObstAvoid:sc::event<Ev_ObstAvoid>{

	};

	struct Ev_UTurn:sc::event<Ev_UTurn>{

	};

	struct Ev_ReachDest:sc::event<Ev_ReachDest>{

	};

	struct Ev_ManualGoal:sc::event<Ev_ManualGoal>{

	};

	/**
	 * ObstAvoid level events
	 */
	struct Ev_ReplanPathFound:sc::event<Ev_ReplanPathFound>{

	};

	/**
	 * UTurn level events
	 */
	struct Ev_UTurnPathFound:sc::event<Ev_UTurnPathFound>{

	};

	/**
	 * ManualGoal level events
	 */

	struct Ev_ManualGoalGot:sc::event<Ev_ManualGoalGot>{

	};
}



#endif /* NAVIEVENTS_HPP_ */
