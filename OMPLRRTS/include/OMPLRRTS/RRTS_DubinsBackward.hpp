/*
 * RRTS_DubinsBackward.hpp
 *
 *  Created on: 16 Apr, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_OMPLRRTS_RRTS_DUBINSBACKWARD_HPP_
#define INCLUDE_OMPLRRTS_RRTS_DUBINSBACKWARD_HPP_

#include <OMPLRRTS/RRTS_Planner.hpp>

namespace MotionPlan {

	class RRTSDubinsBackward:public RRTSPlanner{
	public:
		RRTSDubinsBackward():RRTSPlanner(1){

		}

		~RRTSDubinsBackward(){

		}

		string getPlannerName(){return string("RRTS_Dubins_Backward");};

	private:
	};


}  // namespace MotionPlan



#endif /* INCLUDE_OMPLRRTS_RRTS_DUBINSBACKWARD_HPP_ */
