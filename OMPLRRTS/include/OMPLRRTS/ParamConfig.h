/*
 * ParamConfig.h
 *
 *  Created on: 1 Apr, 2014
 *      Author: liuwlz
 */

#ifndef PARAMCONFIG_H_
#define PARAMCONFIG_H_

#include <vector>

using namespace std;

namespace MotionPlan{

	class ParamConfig{

	public:
		typedef enum { robotHeight=0, robotWidth, wheelDist, safeMargin } ROBOT_PARAM;
		typedef enum { plannerType=0, gammaValue, goalSampleFreq, steerDist, sampleType, collideType} PLANNER_PARAM;
		typedef enum {numDim = 0, regionSize} STATE_PARAM;

		double robotParam[4];
		double plannerParam[5];
		double stateParam[2];

		ParamConfig();
		~ParamConfig();
	};
}

#endif /* PARAMCONFIG_H_ */
