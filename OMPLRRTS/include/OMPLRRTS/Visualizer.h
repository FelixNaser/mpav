/*
 * Visualizer.h
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 */

#ifndef VISUALIZER_H_
#define VISUALIZER_H_

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <std_msgs/ColorRGBA.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/PointCloud.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <ompl/base/PlannerData.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/ProblemDefinition.h>
#include <ompl/base/Path.h>

#include <ompl/geometric/PathGeometric.h>

#include <boost/lexical_cast.hpp>

#include <OMPLRRTS/MotionPlanResult.h>

#define HALF_PI 1.5707963268
#define MAX_VISUAL_DIST 3.0

using namespace std;
using namespace boost;


namespace MotionPlan{

	namespace ob = ompl::base;
	namespace og = ompl::geometric;

	typedef ob::ProblemDefinitionPtr problemDef_ptr;
	typedef ob::PlannerDataPtr plandata_ptr;
	typedef ob::SpaceInformationPtr spaceinfo_ptr;
	typedef ob::StateSpacePtr space_ptr;
	typedef ob::PathPtr path_ptr;
	typedef og::PathGeometric path_geometric_t;
	typedef ob::ReedsSheppStateSpace::ReedsSheppPath RS_path_t;
	typedef boost::shared_ptr<RS_path_t> RS_path_ptr;

	class Visualizer{

		plandata_ptr planner_data_;
		spaceinfo_ptr spaceinfo_;

		ros::NodeHandle nh_, pri_nh_;
		ros::Publisher plan_result_pub_;
		ros::Publisher edge_pub_, vertex_pub_, path_view_pub_, path_car_pub_, path_control_pub_;
		tf::TransformListener tf_;

		string base_frame_, global_frame_;
		double turning_radius_;

	public:
		Visualizer(spaceinfo_ptr spaceinfo);
		virtual ~Visualizer();

		nav_msgs::Path solution_path_;
		visualization_msgs::MarkerPtr path_marker_;

		boost::shared_ptr<OMPLRRTS::MotionPlanResult> plan_result_;

		int viewPlannerData(const plandata_ptr planDataIn, bool plannerRest);
		int parseSolutionPath(const path_ptr pathIn);
		int publishSolutionPath();
		int checkSolutionPath(const path_ptr pathIn);
		int checkDubinsSolutionPath();
		int viewSolutionCar(const path_ptr pathIn);
		int interpolateSolutionPath(const ob::State* stateIn, const RS_path_t& pathIn, vector<ob::State*>& statesOut, vector<int>& controlFlagOut, int numOfState);

		int stateToPose(const ob::State* stateIn, int controlFlag, geometry_msgs::PoseStampedPtr& poseOut);
		int stateToPose(const ob::State* stateIn, geometry_msgs::PoseStampedPtr& poseOut);
		inline void stateToPoint(const ob::State* stateIn, geometry_msgs::PointPtr& pointOut);

		std_msgs::ColorRGBA getScaledRGB(double valueIn, double minIn, double maxIn);
		void initMarker(visualization_msgs::MarkerPtr& markerIn, int markerType);
	};
}

#endif /* VISUALIZER_H_ */
