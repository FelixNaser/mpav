/*
 * RvizGoal.cpp
 *
 *  Created on: 3 Apr, 2014
 *      Author: liuwlz
 *
 *  Naive Implementation of simple goal, which use Rviz to set goal.
 *  Testing purpose only;
 */


#include <OMPLRRTS/RvizGoal.h>

using namespace std;

namespace MotionPlan{

	RvizGoal::
	RvizGoal(){
	      rviz_goal_sub_ = nh_.subscribe("/move_base_simple/goal", 2, &RvizGoal::rvizGoalCallBack, this);
	      rviz_goal_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("/rrts_goal", 1);
	}

	RvizGoal::
	~RvizGoal(){

	}

	void
	RvizGoal::
	rvizGoalCallBack(const geometry_msgs::PoseStamped::ConstPtr& goalIn){
		  ROS_INFO("Received simple goal from rviz");
	      geometry_msgs::PoseStamped pose;
	      pose = *goalIn;
	      rviz_goal_pub_.publish(pose);
	}
}

int main(int argc, char **argv){
 	ros::init(argc, argv, "rviz_goal");
 	MotionPlan::RvizGoal rviz_goal;
 	ros::spin();
 	return 0;
}
