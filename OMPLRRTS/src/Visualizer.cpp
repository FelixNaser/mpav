/*
 * Visualizer.cpp
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 *
 *  Visualize the planner data, e.g. edges, vertices, and solution path using RViz from ROS
 */

#include <OMPLRRTS/Visualizer.h>
namespace MotionPlan{
	Visualizer::Visualizer(spaceinfo_ptr spaceinfo):
		spaceinfo_(spaceinfo),
		pri_nh_("~"){

		pri_nh_.param("base_frame", base_frame_,string("base_link"));
		pri_nh_.param("global_frame", global_frame_,string("map"));
		pri_nh_.param("turning_radius",turning_radius_,3.0);

		plan_result_pub_ = nh_.advertise<OMPLRRTS::MotionPlanResult>("plan_result",1);
		edge_pub_ = nh_.advertise<visualization_msgs::Marker>("planner_edge",1);
		vertex_pub_ = nh_.advertise<visualization_msgs::Marker>("planner_vertex",1);
		path_view_pub_ = nh_.advertise<visualization_msgs::Marker>("trajectory_view",1);
		path_car_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("traj_car_view",2);
		path_control_pub_ = nh_.advertise<nav_msgs::Path>("trajectory_control",1,true);

		plan_result_ = boost::shared_ptr<OMPLRRTS::MotionPlanResult>(new OMPLRRTS::MotionPlanResult());
		path_marker_ = visualization_msgs::MarkerPtr(new visualization_msgs::Marker());
	}

	Visualizer::~Visualizer(){

	}

	int
	Visualizer::
	viewPlannerData(const plandata_ptr planDataIn, bool plannerRest){

		planner_data_ = planDataIn;

		visualization_msgs::MarkerPtr vertex_marker_(new visualization_msgs::Marker());
		visualization_msgs::MarkerPtr edge_marker_(new visualization_msgs::Marker());
		geometry_msgs::PointPtr points_ptr(new geometry_msgs::Point());
		initMarker(vertex_marker_, 1);
		initMarker(edge_marker_, 2);

		int state_num = 0;
		double clearance_sum = 0.0;

		const ob::State *curr_state, *next_state;
		vector<ob::State*> inter_states;

		for( int vertex_id = 0; vertex_id < int( planner_data_->numVertices()); vertex_id++ ){
			//Traverse all the vertices
			curr_state = planner_data_->getVertex(vertex_id).getState();
			if (curr_state){
				stateToPoint(curr_state, points_ptr);
				vertex_marker_->points.push_back(*points_ptr);
				//Traverse all the children
				vector<unsigned int> edge_list;
				planner_data_->getEdges( vertex_id, edge_list );
				for( vector<unsigned int>::const_iterator edge_it = edge_list.begin(); edge_it != edge_list.end(); edge_it++){
					next_state = planner_data_->getVertex(*edge_it).getState();
					if (next_state){
						spaceinfo_->getMotionStates(curr_state, next_state, inter_states, 50, true, true);
						for (vector<ob::State*>::const_iterator state_it = inter_states.begin(); state_it!= inter_states.end(); state_it++){
							double dist_clearence = spaceinfo_->getStateValidityChecker()->clearance(*state_it);
							stateToPoint(*state_it, points_ptr);
							if (state_it == inter_states.begin() || state_it == inter_states.end()-1){
								edge_marker_->points.push_back(*points_ptr);
								edge_marker_->colors.push_back(getScaledRGB(dist_clearence,0,MAX_VISUAL_DIST));
							}
							else{
								edge_marker_->points.push_back(*points_ptr);
								edge_marker_->points.push_back(*points_ptr);
								edge_marker_->colors.push_back(getScaledRGB(dist_clearence,0,MAX_VISUAL_DIST));
								edge_marker_->colors.push_back(getScaledRGB(dist_clearence,0,MAX_VISUAL_DIST));
							}
							clearance_sum += dist_clearence;
							state_num ++;
						}
						inter_states.clear();
					}
					else{
						ROS_ERROR("Wrong children memory allocation.");
						return 0;
					}
				}
			}
			else{
				ROS_ERROR("Wrong vertex memory allocation.");
				return 0;
			}
		}

		plan_result_->NumOfVertex = planner_data_->numVertices();
		plan_result_->NumOfEdge = planner_data_->numEdges();
		plan_result_->PathClearence = clearance_sum/(double)state_num;;

		vertex_pub_.publish(*vertex_marker_);
		edge_pub_.publish(*edge_marker_);
		return 1;
	}

#if 0
	int
	Visualizer::
	ViewSolutionPath(const path_ptr& pathIn){

		path_geometric_t &path_ = static_cast<path_geometric_t&>(*pathIn);
		path_.interpolate(100);
		vector<ob::State*> solution_states;
		solution_states = path_.getStates();

		geometry_msgs::PointPtr points_ptr(new geometry_msgs::Point());
		visualization_msgs::MarkerPtr path_marker_ptr(new visualization_msgs::Marker());
		initMarker(path_marker_ptr,3);

		for (vector<ob::State*>::const_iterator state_it = solution_states.begin(); state_it!= solution_states.end(); state_it++){
			stateToPoint(*state_it, points_ptr);
			if (state_it == solution_states.begin() || state_it == solution_states.end()-1){
				path_marker_ptr->points.push_back(*points_ptr);
			}
			else{
				path_marker_ptr->points.push_back(*points_ptr);
				path_marker_ptr->points.push_back(*points_ptr);
			}
		}
		solution_states.clear();
		path_view_pub_.publish(path_marker_ptr);

		return 1;
	}
#else
	int
	Visualizer::
	parseSolutionPath(const path_ptr pathIn){
		path_geometric_t &path_ = static_cast<path_geometric_t&>(*pathIn);
		vector<ob::State*> solution_key_states;
		solution_key_states = path_.getStates();

		geometry_msgs::PoseStampedPtr pose_tmp = geometry_msgs::PoseStampedPtr(new geometry_msgs::PoseStamped());
		geometry_msgs::PointPtr points_ptr(new geometry_msgs::Point());

		initMarker(path_marker_,3);
		path_marker_->points.clear();

		int path_seq = 0;
		int state_num = 0;
		double clearance_sum = 0.0;
		solution_path_.poses.clear();

		for (vector<ob::State*>::const_iterator
				state_it = solution_key_states.begin();
				state_it!= (solution_key_states.end()-1);
				state_it++){

			ob::State* current_state = *(state_it);
			ob::State* next_state = *(state_it+1);
			vector<ob::State*> solution_states;
			vector<int> controls;

			RS_path_t RS_path = spaceinfo_->getStateSpace()->as<ob::ReedsSheppStateSpace>()->reedsShepp(current_state, next_state);
			this->interpolateSolutionPath(current_state, RS_path, solution_states, controls,75);
			controls[0] = controls[1];

			for (vector<ob::State*>::const_iterator state_it = solution_states.begin();
					state_it!= solution_states.end();
					state_it++){
				//For trajectory_view
				this -> stateToPoint(*state_it, points_ptr);
				if (state_it == solution_states.begin() || state_it == solution_states.end()-1){
					path_marker_->points.push_back(*points_ptr);
				}
				else{
					path_marker_->points.push_back(*points_ptr);
					path_marker_->points.push_back(*points_ptr);
				}
				//For trajectory_control
				this -> stateToPose(*state_it, controls[state_it-solution_states.begin()], pose_tmp);
				pose_tmp->header.seq = path_seq;
				solution_path_.poses.push_back(*pose_tmp);
				path_seq ++;
				clearance_sum += spaceinfo_->getStateValidityChecker()->clearance(*state_it);
				state_num ++;
			}
			solution_states.clear();
		}
		plan_result_pub_.publish(plan_result_);
		return 1;
	}
#endif

	int
	Visualizer::
	checkSolutionPath(const path_ptr pathIn){
		path_geometric_t &path_ = static_cast<path_geometric_t&>(*pathIn);
		vector<ob::State*> solution_key_states;
		solution_key_states = path_.getStates();
		for (vector<ob::State*>::const_iterator
				state_it = solution_key_states.begin();
				state_it!= (solution_key_states.end()-1);
				state_it++){
			ob::State* current_state = *(state_it);
			ob::State* next_state = *(state_it+1);
			vector<ob::State*> solution_states;
			vector<int> controls;

			RS_path_t RS_path = spaceinfo_->getStateSpace()->as<ob::ReedsSheppStateSpace>()->reedsShepp(current_state, next_state);
			this->interpolateSolutionPath(current_state, RS_path, solution_states, controls,75);
			controls[0] = controls[1];
			for (vector<ob::State*>::const_iterator state_it = solution_states.begin();
								state_it!= solution_states.end();
								state_it++){
				if (spaceinfo_->getStateValidityChecker()->clearance(*state_it) == 0.0)
					return 0;
			}
		}
		return 1;
	}

	int
	Visualizer::
	publishSolutionPath(){
		solution_path_.header.stamp = ros::Time::now();
		solution_path_.header.frame_id = global_frame_;
		path_control_pub_.publish(solution_path_);
		path_view_pub_.publish(*path_marker_);
		return 1;
	}

	int
	Visualizer::
	checkDubinsSolutionPath(){
		for (vector<geometry_msgs::PoseStamped>::iterator iter = solution_path_.poses.begin();
				iter != solution_path_.poses.end();
				iter++){
			if (iter ->pose.position.z < 0)
				return 0;
		}
		return 1;
	}

	int
	Visualizer::
	viewSolutionCar(const path_ptr pathIn){
		int carCounter = 0;
		path_geometric_t &path_car_ = static_cast<path_geometric_t&>(*pathIn);
		int numCar = (int)path_car_.length();
		path_car_.interpolate(numCar);
		vector<ob::State*> car_states;
		car_states = path_car_.getStates();

		geometry_msgs::PointPtr points_ptr(new geometry_msgs::Point());
		visualization_msgs::MarkerArrayPtr car_marker_array_ptr(new visualization_msgs::MarkerArray());

		for (vector<ob::State*>::const_iterator state_it = car_states.begin(); state_it!= car_states.end(); state_it++){
			visualization_msgs::MarkerPtr car_marker_ptr(new visualization_msgs::Marker());
			initMarker(car_marker_ptr, 4);
			stateToPoint(*state_it, points_ptr);

			car_marker_ptr->ns = boost::lexical_cast<string>(carCounter);
			car_marker_ptr->pose.position.x = points_ptr->x + 2.0*cos(points_ptr->z) + 0.5*sin(points_ptr->z);
			car_marker_ptr->pose.position.y = points_ptr->y + 2.0*sin(points_ptr->z) - 0.5*cos(points_ptr->z);
			car_marker_ptr->pose.orientation.z = sin(points_ptr->z/2.0 + HALF_PI/2.0);
			car_marker_ptr->pose.orientation.w = cos(points_ptr->z/2.0 + HALF_PI/2.0);

			carCounter++;
			car_marker_array_ptr->markers.push_back(*car_marker_ptr);
		}
		path_car_pub_.publish(car_marker_array_ptr);
		return 1;
	}

	int
	Visualizer::
	interpolateSolutionPath(const ob::State* stateIn, const RS_path_t& pathIn, vector<ob::State*>& statesOut, vector<int>& controlFlagOut, int numOfState){

		double radius = turning_radius_;
		double increment = 0.0;
		const ob::ReedsSheppStateSpace::StateType *state_from = static_cast<const ob::ReedsSheppStateSpace::StateType*>(stateIn);

		while (increment < 1.0 + 1e-8){

			double seg =  increment * pathIn.length();
			double theta, variance;
			int control_flag = 0;

			ob::State* state_out = spaceinfo_->allocState();
			ob::ScopedState<ob::ReedsSheppStateSpace> state_tmp(spaceinfo_->getStateSpace());

			state_tmp->setXY(0.0, 0.0);
			state_tmp->setYaw(state_from->getYaw());

			for (size_t i=0; i<5 && seg>0; ++i){
				if (pathIn.length_[i]<0){
					variance = max(-seg, pathIn.length_[i]);
					control_flag = -1;
					seg += variance;
				}
				else{
					variance = min(seg, pathIn.length_[i]);
					control_flag = 1;
					seg -= variance;
				}

				theta = state_tmp->getYaw();
				switch(pathIn.type_[i]){
				case 1: /*RS_LEFT*/
					state_tmp->setXY(state_tmp->getX() + sin(theta+variance) - sin(theta), state_tmp->getY() - cos(theta+variance) + cos(theta));
					state_tmp->setYaw(theta + variance);
					break;
				case 3:/*RS_RIGHT*/
					state_tmp->setXY(state_tmp->getX() - sin(theta-variance) + sin(theta), state_tmp->getY() + cos(theta-variance) - cos(theta));
					state_tmp->setYaw(theta - variance);
	           	    break;
				case 2:/*RS_STRAIGHT*/
	                state_tmp->setXY(state_tmp->getX() + variance * cos(theta), state_tmp->getY() + variance * sin(theta));
	                break;
	            case 0:
	            	break;
				}
			}
			state_out->as<ob::ReedsSheppStateSpace::StateType>()->setX(state_tmp->getX() * radius + state_from->getX());
			state_out->as<ob::ReedsSheppStateSpace::StateType>()->setY(state_tmp->getY() * radius + state_from->getY());
			state_out->as<ob::ReedsSheppStateSpace::StateType>()->setYaw(state_tmp->getYaw());
			increment = increment + 1/(double)numOfState;
			statesOut.push_back(state_out);
			controlFlagOut.push_back(control_flag);
		}
		return 1;
	}

	int
	Visualizer::
	stateToPose(const ob::State* stateIn, geometry_msgs::PoseStampedPtr& poseOut){
	    const ob::SE2StateSpace::StateType *real_state = static_cast<const ob::SE2StateSpace::StateType*>(stateIn);
	    geometry_msgs::PoseStampedPtr pose_tmp = geometry_msgs::PoseStampedPtr(new geometry_msgs::PoseStamped());
	    pose_tmp->pose.position.x = real_state->getX();
	    pose_tmp->pose.position.y = real_state->getY();
	    pose_tmp->pose.position.z = real_state->getYaw();
	    pose_tmp->pose.orientation.w = 1.0;
	    pose_tmp->header.frame_id = global_frame_;
	    pose_tmp->header.stamp = ros::Time::now();
	    poseOut = pose_tmp;
	    return 1;
	}

	int
	Visualizer::
	stateToPose(const ob::State* stateIn, int controlFlag, geometry_msgs::PoseStampedPtr& poseOut){
	    const ob::SE2StateSpace::StateType *real_state = static_cast<const ob::SE2StateSpace::StateType*>(stateIn);
	    geometry_msgs::PoseStampedPtr pose_tmp = geometry_msgs::PoseStampedPtr(new geometry_msgs::PoseStamped());
	    pose_tmp->pose.position.x = real_state->getX();
	    pose_tmp->pose.position.y = real_state->getY();
	    pose_tmp->pose.position.z = controlFlag;
	    pose_tmp->pose.orientation = tf::createQuaternionMsgFromYaw(real_state->getYaw());
	    pose_tmp->header.frame_id = global_frame_;
	    pose_tmp->header.stamp = ros::Time::now();
	    poseOut = pose_tmp;
	    return 1;
	}

	inline void
	Visualizer::
	stateToPoint(const ob::State* stateIn, geometry_msgs::PointPtr& pointOut){
	    geometry_msgs::PoseStampedPtr pose_tmp = geometry_msgs::PoseStampedPtr(new geometry_msgs::PoseStamped());
		stateToPose(stateIn, pose_tmp);
		pointOut->x = pose_tmp->pose.position.x;
		pointOut->y = pose_tmp->pose.position.y;
		pointOut->z = pose_tmp->pose.position.z;
	}

	//Modified from http://stackoverflow.com/questions/7706339/grayscale-to-red-green-blue-matlab-jet-color-scale
	std_msgs::ColorRGBA
	Visualizer::
	getScaledRGB(double valueIn, double minIn, double maxIn){
		std_msgs::ColorRGBA scaled_color;
		scaled_color.r = scaled_color.g = scaled_color.b = 1.0;
		double value_diff = maxIn - minIn;

		valueIn = valueIn < minIn ? minIn : valueIn;
		valueIn = valueIn > maxIn ? maxIn : valueIn;

		valueIn = maxIn - valueIn;

		if (valueIn < (minIn + 0.25 * value_diff)) {
			scaled_color.r = 0;
			scaled_color.g = 4 * (valueIn - minIn) / value_diff;
		} else if (valueIn < (minIn + 0.5 * value_diff)) {
			scaled_color.r = 0;
			scaled_color.b = 1 + 4 * (minIn + 0.25 * value_diff - valueIn) / value_diff;
		} else if (valueIn < (minIn + 0.75 * value_diff)) {
			scaled_color.r = 4 * (valueIn - minIn - 0.5 * value_diff) / value_diff;
			scaled_color.b = 0;
		} else {
			scaled_color.g = 1 + 4 * (minIn + 0.75 * value_diff - valueIn) / value_diff;
			scaled_color.b = 0;
		}
		return scaled_color;
	}

	void
	Visualizer::
	initMarker(visualization_msgs::MarkerPtr& markerIn, int markerType){
		markerIn->header.frame_id = global_frame_;
		markerIn->header.stamp = ros::Time::now();
		markerIn->action = visualization_msgs::Marker::ADD;
		markerIn->lifetime = ros::Duration(0.0);
		if (markerType == 1){
			markerIn->ns = "Vertex";
			markerIn->type = visualization_msgs::Marker::SPHERE_LIST;
			markerIn->scale.x = 0.2;
			markerIn->scale.y = 0.2;
			markerIn->scale.z = 0.02;

			markerIn->color.r = 1.0;
			markerIn->color.g = 0.0;
			markerIn->color.b = 0.0;
			markerIn->color.a = 1.0;
		}

		if (markerType == 2){
			markerIn->ns = "Edge";
			markerIn->type = visualization_msgs::Marker::LINE_LIST;
			markerIn->scale.x = 0.03;
			markerIn->scale.y = 1.0;
			markerIn->scale.z = 1.0;
			/**
			markerIn->color.r = 0.0;
			markerIn->color.g = 0.0;
			markerIn->color.b = 1.0;
			*/
			markerIn->color.a = 1.0;
		}

		if (markerType == 3){
			markerIn->ns = "Trajectory";
			markerIn->type = visualization_msgs::Marker::LINE_LIST;
			markerIn->scale.x = 0.03;
			markerIn->scale.y = 1.0;
			markerIn->scale.z = 1.0;

			markerIn->color.r = 0.0;
			markerIn->color.g = 1.0;
			markerIn->color.b = 0.0;
			markerIn->color.a = 0.8;
		}

		if (markerType == 4){
			markerIn->type = visualization_msgs::Marker::MESH_RESOURCE;
			markerIn->mesh_resource = "package://local_map/urdf/SCOT_bin.stl";
			markerIn->scale.x = 0.6;
			markerIn->scale.y = 0.6;
			markerIn->scale.z = 0.6;

			markerIn->color.r = 0.0;
			markerIn->color.g = 1.0;
			markerIn->color.b = 0.0;
			markerIn->color.a = 1.0;
		}

		markerIn->pose.position.x = 0;
		markerIn->pose.position.y = 0;
		markerIn->pose.position.z = 0;
		markerIn->pose.orientation.x = 0.0;
		markerIn->pose.orientation.y = 0.0;
		markerIn->pose.orientation.z = 0.0;
		markerIn->pose.orientation.w = 1.0;
	}
}
