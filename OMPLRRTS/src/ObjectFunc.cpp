/*
 * ObjectFunc.cpp
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 */

#include <OMPLRRTS/ObjectFunc.h>

namespace MotionPlan{

	ObjectFunc::ObjectFunc(const ob::SpaceInformationPtr& si, bool checkMotion, double bwd_penalty_ratio):
		ob::StateCostIntegralObjective(si, checkMotion),
		si_(si),
		bwd_penalty_ratio_(bwd_penalty_ratio){
	}

	ObjectFunc::~ObjectFunc(){

	}

	ob::Cost ObjectFunc::stateCost(const ob::State* stateIn) const{
		return ob::Cost(1 / si_->getStateValidityChecker()->clearance(stateIn));
	}

	ob::Cost ObjectFunc::motionCost(const ob::State* s1, const ob::State* s2) const{
		if (interpolateMotionCost_){
	        ob::Cost totalCost = this->identityCost();
	        int nd = si_->getStateSpace()->validSegmentCount(s1, s2);
	        RS_path_t RS_path = si_->getStateSpace()->as<ob::ReedsSheppStateSpace>()->reedsShepp(s1, s2);
	        ob::State *test1 = si_->cloneState(s1);
	        ob::Cost pre_state_cost = this->stateCost(test1);
	        if (nd > 1){
	            ob::State *test2 = si_->allocState();
	            for (int j = 1; j < nd; ++j){
	                interpolateRSPath(s1, RS_path, (double)j/(double)nd, *test2);
	                double next_cost_tmp = test2->as<ob::ReedsSheppStateSpace::StateType>()->getYaw() > 0 ?
	                		this->stateCost(test2).value() : bwd_penalty_ratio_*this->stateCost(test2).value();
	                ob::Cost next_stat_cost = ob::Cost(next_cost_tmp);
	                totalCost = ob::Cost(totalCost.value() + this->trapezoid(pre_state_cost, next_stat_cost, si_->distance(test1, test2)).value());
	                swap(test1, test2);
	                pre_state_cost = next_stat_cost;
	            }
	            si_->freeState(test2);
	        }
	        totalCost = ob::Cost( totalCost.value() + this->trapezoid(pre_state_cost, this->stateCost(s2), si_->distance(test1, s2)).value());
	        si_->freeState(test1);
	        return totalCost;
	    }
	    else
	        return this->trapezoid(this->stateCost(s1), this->stateCost(s2), si_->distance(s1, s2));
	}

	int
	ObjectFunc::
	interpolateRSPath(const ob::State* stateFrom, const RS_path_t& pathIn, double t, ob::State& stateOut)const{
		double radius = 3.0; /**<Turning radius of ReedShepp path*/
		double seg =  t * pathIn.length();
		double theta, variance, yaw_tmp;

		ob::State* state_tmp = si_->allocState();

		state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setXY(0.0, 0.0);
		state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setYaw(1.0);
		yaw_tmp = stateFrom->as<ob::ReedsSheppStateSpace::StateType>()->getYaw();

		for (size_t i=0; i<5 && seg>0; ++i){
			if (pathIn.length_[i]<0){
				variance = max(-seg, pathIn.length_[i]);
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setYaw(-1.0);
				seg += variance;
			}
			else{
				variance = min(seg, pathIn.length_[i]);
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setYaw(1.0);
				seg -= variance;
			}

			theta = yaw_tmp;
			switch(pathIn.type_[i]){
			case 1: /**\brief RS_LEFT*/
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setXY(
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getX() + sin(theta+variance) - sin(theta),
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getY() - cos(theta+variance) + cos(theta));
				yaw_tmp = theta + variance;
				break;
			case 3:/**\brief RS_RIGHT*/
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setXY(
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getX() - sin(theta-variance) + sin(theta),
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getY() + cos(theta-variance) - cos(theta));
				yaw_tmp = theta - variance;
				break;
			case 2:/**\brief RS_STRAIGHT*/
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->setXY(
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getX() + variance * cos(theta),
						state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getY() + variance * sin(theta));
				break;
			case 0:
				break;
			}
		}
		stateOut.as<ob::ReedsSheppStateSpace::StateType>()->setX(
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getX() * radius +
				stateFrom->as<ob::ReedsSheppStateSpace::StateType>()->getX());
		stateOut.as<ob::ReedsSheppStateSpace::StateType>()->setY(
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getY() * radius +
				stateFrom->as<ob::ReedsSheppStateSpace::StateType>()->getY());
		stateOut.as<ob::ReedsSheppStateSpace::StateType>()->setYaw(
				state_tmp->as<ob::ReedsSheppStateSpace::StateType>()->getYaw());

		si_->freeState(state_tmp);
		return 1;
	}

}
