/*
 * main.cpp
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 */

#include <OMPLRRTS/RRTS_Planner.hpp>

int main(int argc, char**argv){
	ros::init(argc, argv,"RRTS");
	if (argc < 2){
		cerr<<"Please Indicate Planner Type"<<endl;
		return 0;
	}
	MotionPlan::RRTSPlanner rrts(atoi(argv[1]));
	ros::spin();
	return 0;
}
