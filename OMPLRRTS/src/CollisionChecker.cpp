/*
 * CollisionChecker.cpp
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 */

#include <OMPLRRTS/CollisionChecker.h>

namespace MotionPlan{

	CollisionChecker::
	CollisionChecker(const ob::SpaceInformationPtr& si, const metric_map_ptr& metricMap):
			ob::StateValidityChecker(si),
			metric_map_(metricMap){

		config_ = boost::shared_ptr<ParamConfig>(new ParamConfig());
		width = config_->robotParam[ParamConfig::robotWidth];
		height = config_->robotParam[ParamConfig::robotHeight];
		dist_rear = config_->robotParam[ParamConfig::wheelDist];
		safety_gap = config_->robotParam[ParamConfig::safeMargin];
	}

	CollisionChecker::
	~CollisionChecker(){

	}

	bool
	CollisionChecker::
	isValid(const state_t* stateIn) const{
		assert(metric_map_ != NULL);
		return (this->clearance(stateIn) > 0.0 && si_->satisfiesBounds(stateIn));
	}

	double
	CollisionChecker::
	clearance(const state_t* stateIn) const{
		const SE2State_t* stateSE2 = stateIn->as<SE2State_t>();
		return getClearance(stateSE2);
	}

	void
	CollisionChecker::
	transformToMetricMapFrame(const SE2State_t* stateIn, vector<double>& poseOut)const{
		poseOut.resize(3);
		double cos_map_yaw = cos(metric_map_->info.origin.position.z);
		double sin_map_yaw = sin(metric_map_->info.origin.position.z);

		poseOut[0] = (stateIn->getX() - metric_map_->info.origin.position.x)*cos_map_yaw + (stateIn->getY() - metric_map_->info.origin.position.y)*sin_map_yaw;
		poseOut[1] = -(stateIn->getX() - metric_map_->info.origin.position.x)*sin_map_yaw + (stateIn->getY()  - metric_map_->info.origin.position.y)*cos_map_yaw;
		poseOut[2] = stateIn->getYaw() - metric_map_->info.origin.position.z;
	}

	int
	CollisionChecker::
	getStateIndex(const double& x, const double& y, int& indexOut)const{
		/*x and y is inverse with index_x and index_y due to ROS coordinate definition*/
		int index_x = (int)round(x/metric_map_->info.resolution);
		int index_y = (int)round(y/metric_map_->info.resolution);

		if( (index_x < 0) || (index_x >= (int)metric_map_->info.width))
			return 0;
		if ( (index_y < 0) || (index_y >= (int)metric_map_->info.height))
			return 0;
		indexOut = index_y*metric_map_->info.width + index_x;
		return 1;
	}

	double
	CollisionChecker::
	getClearance(const SE2State_t* stateIn)const{
		vector<double> map_pose;
		this->transformToMetricMapFrame(stateIn,map_pose);

		double xc = map_pose[0], yc = map_pose[1], yaw = map_pose[2];
		double cos_yaw = cos(yaw), sin_yaw = sin(yaw);
		double min_clearance = DBL_MAX;

		double cxmax =  height - dist_rear + safety_gap + 0.001;
		double cymax = width/2.0 + safety_gap + 0.001;
		double cy = -width/2.0 - safety_gap;

		//Check each pose within the vehicle geometric footprint.
		while(cy < cymax){
			double cx = -dist_rear - safety_gap;
		    while(cx < cxmax){
		    	double x = xc + cx*cos_yaw - cy*sin_yaw;
		    	double y = yc + cx*sin_yaw + cy*cos_yaw;
		    	int index = -1;
		    	if(getStateIndex(x, y, index) == 1){
		    		double temp_clearance = metric_map_->metric[index];
		    		if (temp_clearance == 0.0)
		    			return 0.0;
		    		min_clearance = temp_clearance < min_clearance ? temp_clearance : min_clearance;
		    	}
		    	else{
		    		return 0.0;
		    	}
		    	cx = cx + metric_map_->info.resolution;
		    }
		    cy = cy + metric_map_->info.resolution;
		}
		return min_clearance;
	}
}
