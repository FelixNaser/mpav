/*
 * OMPLRRTS.cpp
 *
 *  Created on: 28 Mar, 2014
 *      Author: liuwlz
 */

#include <OMPLRRTS/OMPLRRTS.h>

namespace MotionPlan{

	OMPLRRTS::
	OMPLRRTS(space_ptr space):
		space_(space),
		planner_states_num_(0),
		lower_bound_metric_(DBL_MAX),
		cost_prefer_ratio_(1.0),
		bwd_penalty_ratio_(5.0),
		planner_reset_(true){
		setupPlanner();
	}

	OMPLRRTS::
	~OMPLRRTS(){

	}

	ob::ValidStateSamplerPtr
	allocVelStateSampler(const ob::SpaceInformation *spaceInfoIn){
		return ob::ValidStateSamplerPtr(new ob::MaximizeClearanceValidStateSampler(spaceInfoIn));
	}

	void
	OMPLRRTS::
	setupPlanner(){
		ROS_INFO("Setup Planner");

		spaceinfo_ = spaceinfo_ptr(new spaceinfo_t(space_));

		root_ = state_ptr(new state_t(space_));
		goal_ = state_ptr(new state_t(space_));

		viewer_ = boost::shared_ptr<Visualizer>(new Visualizer(spaceinfo_));
		planner_= planner_ptr(new og::RRTstar(spaceinfo_));
		planner_->as<og::RRTstar>()->setNearestNeighbors<ompl::NearestNeighborsSqrtApprox>();

		spaceinfo_->setValidStateSamplerAllocator(allocVelStateSampler);

		if (spaceinfo_->getStateSpace()->getName().compare(string("Dubins")) == 0)
			vertex_bound_num_ = 2;
		else
			vertex_bound_num_ = 50;
	}

	void
	OMPLRRTS::
	resetPlanner(){
		ROS_INFO("Reset Planner");
		assert(metric_map_ != NULL);

		pdef_ = problemDef_ptr(new problemDef_t(spaceinfo_));
		pdef_->setOptimizationObjective(getBalancedObjective());
		pdef_->setStartAndGoalStates(*root_, *goal_, 0.5);

		planner_->clear();
		planner_->setProblemDefinition(pdef_);
		planner_->setup();

		plan_data_ = plandata_ptr(new plandata_t(spaceinfo_));

		planner_states_num_ = 0;
		lower_bound_metric_ = DBL_MAX;
		planner_reset_ = true;
	}

	void
	OMPLRRTS::
	setPlannerParams(double goal_bias, double steer_dist){
		planner_->as<og::RRTstar>()->setGoalBias(0.25);
		planner_->as<og::RRTstar>()->setRange(3.0);
		planner_->as<og::RRTstar>()->setDelayCC(true);
	}

	int
	OMPLRRTS::
	iteratePlan(double duration){
		try{
		  plan_status_ = planner_->solve(duration);
		}
		catch(std::exception& e){
		    ROS_ERROR("Planner Error");
		    return 5;
		}
		/*Visualize planning data, Disabled by default to save computation and memory*/
		planner_->getPlannerData(*plan_data_);
		planner_states_num_ = plan_data_->numVertices();
		viewer_->viewPlannerData(plan_data_, planner_reset_);
		planner_reset_ = false;

		//TODO: Adaptively change the planning criteria, such as planning_states_num, etc.
		if (plan_status_ == ob::PlannerStatus::StatusType(ob::PlannerStatus::EXACT_SOLUTION) && planner_states_num_> vertex_bound_num_){
			viewer_->parseSolutionPath(pdef_->getSolutionPath());
			lower_bound_metric_ = pdef_->getSolutionPath()->cost(getBalancedObjective()).value();
			ROS_INFO("Found exact solution of path cost: %f , Number of Vertex: %d", lower_bound_metric_, planner_states_num_);
			return 0;
		}
		else if (plan_status_  ==  ob::PlannerStatus::StatusType(ob::PlannerStatus::INVALID_START)){
			ROS_WARN("Vehicle In Collision");
			return 1;
		}
		else if (plan_status_  ==  ob::PlannerStatus::StatusType(ob::PlannerStatus::INVALID_GOAL)){
			ROS_WARN("Invalid Goal");
			return 2;
		}
		else if (plan_status_  ==  ob::PlannerStatus::StatusType(ob::PlannerStatus::TIMEOUT)){
			ROS_WARN("OMPL Timeout");
			return 3;
		}
		else{
			return 4;
		}

		/**< Some Notes:
		PlannerStatus:
			UNKNOWN(0) 					Uninitialized status.
			INVALID_START(1) 			Invalid start state or no start state specified.
			INVALID_GOAL(2)				Invalid goal state.
			UNRECOGNIZED_GOAL_TYPE(3)	The goal is of a type that a planner does not recognize.
			TIMEOUT(4) 					The planner failed to find a solution.
			APPROXIMATE_SOLUTION(5) 	The planner found an approximate solution.
			EXACT_SOLUTION(6) 			The planner found an exact solution.
			CRASH(7) 					The planner crashed.
			TYPE_COUNT(8) 				The number of possible status values.
		*/
	}

	object_ptr
	OMPLRRTS::
	getBalancedObjective(){
		object_ptr lengthObj(new ob::PathLengthOptimizationObjective(spaceinfo_));
#if 0
		object_ptr clearObj(new ObjectFunc(spaceinfo_, false, bwd_penalty_ratio_));

		ob::MultiOptimizationObjective* multiObj = new ob::MultiOptimizationObjective(spaceinfo_);
		multiObj->addObjective(lengthObj, 1.0);
		multiObj->addObjective(clearObj, 1.0*cost_prefer_ratio_);
		object_ptr balancedObj = object_ptr(multiObj);

		return balancedObj;
#endif
		return lengthObj;
	}

	bool
	OMPLRRTS::
	lazyCheckTree(){
		if (viewer_->checkSolutionPath(pdef_->getSolutionPath()) == 0){
			ROS_WARN("Commit path unsafe, replan trigger!");
			return false;
		}
		else
			return true;
	}

	void
	OMPLRRTS::
	resetRoot(const vector<double>& rootIn){
		for (size_t i = 0 ; i < spaceinfo_->getStateDimension(); i++)
			(*root_)[i] = rootIn[i];
		return;
	}

	void
	OMPLRRTS::
	resetGoal(const vector<double>& goalIn){
		for (size_t i = 0 ; i < spaceinfo_->getStateDimension(); i++)
			(*goal_)[i] = goalIn[i];
	}

	void
	OMPLRRTS::
	resetBounds(const ob::RealVectorBounds& boundsIn){
		spaceinfo_->getStateSpace()->as<ob::SE2StateSpace>()->setBounds(boundsIn);
	}

	void
	OMPLRRTS::
	resetMetricMap(const metric_map_ptr& metricMapIn, bool resetPlannerFlag){
		metric_map_ = metricMapIn;
		spaceinfo_->setStateValidityChecker(ob::StateValidityCheckerPtr(new CollisionChecker(spaceinfo_, metric_map_)));
		if (spaceinfo_->getStateSpace()->getName().compare(string("Dubins")) == 0){
			spaceinfo_->setMotionValidator(ob::MotionValidatorPtr(new ob::DubinsMotionValidator(spaceinfo_)));
		}
		spaceinfo_->setup();
		if (resetPlannerFlag){
			this->resetPlanner();
		}
	}

	nav_msgs::Path
	OMPLRRTS::
	getSolutionPath() const{
		return viewer_->solution_path_;
	}
}

