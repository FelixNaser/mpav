/*
 * Replan_Trigger.hpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */

#ifndef REPLAN_TRIGGER_HPP_
#define REPLAN_TRIGGER_HPP_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud.h>
#include <nav_msgs/Path.h>

using namespace std;
using namespace boost;

namespace Perception {
	class ReplanTrigger {
	public:
		ReplanTrigger();
		virtual ~ReplanTrigger();

		double distBetweenPosewithPoint(const geometry_msgs::PoseStamped& poseIn, const geometry_msgs::Point32& pointIn);

		ros::NodeHandle nh_,pri_nh_;
		string global_frame_,base_frame_;

		tf::TransformListener tf_;

		typedef enum{Obst,Parking,UTurn,Disable, Warn}TriggerType;
		TriggerType trigger_status_;
	};
}  // namespace Perception


#endif /* REPLAN_TRIGGER_HPP_ */
