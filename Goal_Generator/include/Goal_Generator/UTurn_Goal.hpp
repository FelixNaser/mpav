/*
 * UTurn_Goal.hpp
 *
 *  Created on: 25 Aug, 2014
 *      Author: liuwlz
 */

#ifndef UTURN_GOAL_HPP_
#define UTURN_GOAL_HPP_

#include <ros/ros.h>
#include <ros/console.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <Goal_Generator/Goal_Generator.hpp>

namespace MotionPlan{

	class UTurnGoal:public GoalGenerator{

	public:
		UTurnGoal();
		virtual ~UTurnGoal();
		void makeUTurnGoal();

		ros::Publisher uturn_goal_pub_;
		geometry_msgs::PoseStamped uturn_goal_;
		nav_msgs::Path remain_path_;
		int goal_type_;

		void setGoalType(int goalTypeIn){goal_type_ = goalTypeIn;};
		void getNearestWaypoint(geometry_msgs::PoseStamped PoseIn);
	};
}

#endif /* UTURN_GOAL_HPP_ */
