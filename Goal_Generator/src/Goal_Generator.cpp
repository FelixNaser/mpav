/*
 * Goal_Generator.cpp
 *
 *  Created on: 11 Aug, 2014
 *      Author: liuwlz
 */


#include <Goal_Generator/Goal_Generator.hpp>

namespace MotionPlan {

	GoalGenerator::
	GoalGenerator():
	pri_nh_("~"),
	waypoint_no_(0),
	vehicle_path_dist_(DBL_MAX){
		pri_nh_.param("global_frame",global_frame_,string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
		pri_nh_.param("waypoint_increment",waypoint_increment_,0.25);

		reference_path_ = shared_ptr<nav_msgs::Path>(new nav_msgs::Path());
	}

	GoalGenerator::
	~GoalGenerator(){

	}

	void
	GoalGenerator::
	initReferencePath(const nav_msgs::Path pathIn){
		assert(pathIn.poses.size()>0);
		denseReferencePath(pathIn);
		waypoint_no_ = 0;
	}

	void
	GoalGenerator::
	denseReferencePath(nav_msgs::Path pathIn){
		reference_path_->poses.clear();
		for (vector<geometry_msgs::PoseStamped>::iterator iter = pathIn.poses.begin();
				iter != pathIn.poses.end()-1;
				iter ++){
			reference_path_->poses.push_back(*iter);
			int path_seg_size = (int)(distBetweenPose(*iter,*(iter+1)) / waypoint_increment_) + 1;
			if (path_seg_size > 1){
				vector<geometry_msgs::PoseStamped> pose_set_insert(path_seg_size - 1);
				for (vector<geometry_msgs::PoseStamped>::iterator iter_insert = pose_set_insert.begin();
						iter_insert != pose_set_insert.end();
						iter_insert ++){
					int curr_index = iter_insert - pose_set_insert.begin() + 1;
					iter_insert->pose.position.x = iter->pose.position.x +
							(double)curr_index/(double)path_seg_size *
							((iter+1)->pose.position.x - iter->pose.position.x);
					iter_insert->pose.position.y = iter->pose.position.y +
							(double)curr_index/(double)path_seg_size *
							((iter+1)->pose.position.y - iter->pose.position.y);
				}
				reference_path_->poses.insert(reference_path_->poses.end(),pose_set_insert.begin(),pose_set_insert.end());
			}
		}
	}

	bool
	GoalGenerator::
	transformGoalPose(Pose poseIn, Pose &poseOut){
		poseIn.header.stamp = ros::Time();
		try{
			tf_.waitForTransform(global_frame_, base_frame_, ros::Time(0), ros::Duration(1.0));
			tf_.transformPose(global_frame_, poseIn, poseOut);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		return true;
	}

	bool
	GoalGenerator::
	getVehiclePose(){
		geometry_msgs::PoseStamped base_pose;
		base_pose.pose.position.x = 0.0;
		base_pose.pose.position.y = 0.0;
		base_pose.pose.orientation.w = 1.0;
		base_pose.header.frame_id = base_frame_;
		base_pose.header.stamp = ros::Time();
		vehicle_pose_.header.stamp = ros::Time();
		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPose(global_frame_, base_pose, vehicle_pose_);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		return true;
	}

	void
	GoalGenerator::
	getNearestWaypoint(){
		ros::Rate loop_rate(1);
		while(!getVehiclePose()){
			ros::spinOnce();
			loop_rate.sleep();
			ROS_INFO("Waiting for Robot pose");
		}
		double min_dist = DBL_MAX;
		int min_index = -1;
		for( vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_->poses.begin() + waypoint_no_;
				iter != reference_path_->poses.end();
				iter++ ){
			double linear_dist = Distance(vehicle_pose_.pose.position.x, vehicle_pose_.pose.position.y,
					iter->pose.position.x, iter->pose.position.y);
			if(linear_dist < min_dist){
				min_index = iter - reference_path_->poses.begin();
				min_dist = linear_dist;
			}
		}
		waypoint_no_ = min_index;
		ROS_INFO("Initial Pose with WayPoint Node: %d", waypoint_no_);
		vehicle_path_dist_ = min_dist;
	}

	void
	GoalGenerator::
	getNearestWaypoint(geometry_msgs::PoseStamped PoseIn){
		ROS_INFO("Check Nearest Waypoint for Pose: %f, %f", PoseIn.pose.position.x, PoseIn.pose.position.y);
		double min_dist = DBL_MAX;
		int min_index = -1;
		for( vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_->poses.begin();
				iter != reference_path_->poses.end();
				iter++ ){
			double linear_dist = Distance(PoseIn.pose.position.x, PoseIn.pose.position.y,
					iter->pose.position.x, iter->pose.position.y);
			if(linear_dist < min_dist){
				min_index = iter - reference_path_->poses.begin();
				min_dist = linear_dist;
			}
		}
		waypoint_no_ = min_index;
		vehicle_path_dist_ = min_dist;
	}

	double
	GoalGenerator::
	distBetweenPose(const geometry_msgs::PoseStamped& poseA, const geometry_msgs::PoseStamped& poseB){
		double x_A = poseA.pose.position.x, y_A = poseA.pose.position.y;
		double x_B = poseB.pose.position.x, y_B = poseB.pose.position.y;
		return sqrt((x_A-x_B)*(x_A-x_B)+(y_A-y_B)*(y_A-y_B));
	}

	double
	GoalGenerator::
	boundAnglePNPI(double angleIn){
		while (angleIn < -M_PI)
			angleIn += 2 * M_PI;
		while (angleIn > M_PI)
			angleIn -= 2*M_PI;
		return angleIn;
	}

	double
	GoalGenerator::
	getYawOfPose(const geometry_msgs::PoseStamped& poseIn){
		double roll=0.0, pitch=0.0, yaw=0.0;
		tf::Quaternion quaternion;
		tf::quaternionMsgToTF(poseIn.pose.orientation, quaternion);
		tf::Matrix3x3(quaternion).getRPY(roll, pitch, yaw);
		return(yaw);
	}

}  // namespace MotionPlan
