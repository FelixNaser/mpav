/*
 * Simulate_Speed_Advisor.hpp
 *
 *  Created on: 7 Aug, 2014
 *      Author: liuwlz
 */

#ifndef SIMULATE_SPEED_ADVISOR_HPP_
#define SIMULATE_SPEED_ADVISOR_HPP_

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>
#include <tf/transform_listener.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <float.h>
#include <math.h>

using namespace std;
using namespace boost;

#define Debug false
#define DECSpeed true

namespace Control {

	class SimulateSpeedAdvisor {

	public:
		SimulateSpeedAdvisor();
		virtual ~SimulateSpeedAdvisor();

		double getAdvisedSpeed()const {return advised_speed_;};

	private:
		ros::NodeHandle nh_, pri_nh_;
		ros::Subscriber simulate_path_sub_, fused_scan_sub_;
		ros::Publisher risk_tube_pub_;
		tf::TransformListener tf_;
		nav_msgs::Path simulate_path_;
		sensor_msgs::PointCloud fused_scan_;
		double max_speed_, advised_speed_;
		double tube_width_, dead_zone_range_, max_speed_range_, max_dec_;
		double obst_dist_;
		bool fused_scan_got_;
		int dynamic_speed_type_;

		string global_frame_, base_frame_;

		void simulatePathCallBack(const nav_msgs::PathPtr pathIn);
		void fusedScanCallBack(const sensor_msgs::PointCloudPtr scanIn);
		void calculateAdvisedSpeed();
		void boundSpeed(double& speedIn);
		void visualizeRiskTube(int riskPoseIndexIn);
		void makeMarker(visualization_msgs::MarkerPtr& markerIn, int markerType);

		double getObstDist(){return obst_dist_;};

		double distBetweenPosewithPoint(const geometry_msgs::PoseStamped& poseIn, const geometry_msgs::Point32& pointIn);
		double distBetweenPosewithPose(const geometry_msgs::PoseStamped& poseInA, const geometry_msgs::PoseStamped& poseInB);
	};

}  // namespace Control


#endif /* SIMULATE_SPEED_ADVISOR_HPP_ */
