/*
 * Speed_Control.hpp
 *
 *  Created on: 2 Aug, 2014
 *      Author: liuwlz
 */

#ifndef SPEED_CONTROL_HPP_
#define SPEED_CONTROL_HPP_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>

#include <Speed_Control/Simulate_Speed_Advisor.hpp>

#include <stdlib.h>
#include <string>

using namespace std;
using namespace boost;

namespace Control{

	template<class AdvisedSpeedType>
	class SpeedControl{

	public:
		SpeedControl();
		virtual ~SpeedControl();

		void SpeedControlTimer(const ros::TimerEvent &e);
		void NormalPlanSpeedControl();
		void ReplanSpeedControl();
		void SlowMoveSpeedControl();
		void TempStopControl();
		void SetStatus(int status);

	    shared_ptr<AdvisedSpeedType> advised_speed_reasoner_;

		ros::NodeHandle nh_, pri_nh_;
	    ros::Subscriber odom_sub_;
	    ros::Publisher cmd_vel_pub_;
	    ros::Timer speed_control_timer_;
	    ros::Time filter_time_;
	    tf::TransformListener tf_;
	    geometry_msgs::TwistStamped cmd_vel_;

	    typedef enum{Normal, SlowMove, Replan, TempStop} SPEEDSTATUS;
	    SPEEDSTATUS speed_state_;

	    string base_frame_, global_frame_;

	    double replan_vel_;
	    double slow_move_ratio_;
	};

}

#endif /* SPEED_CONTROL_HPP_ */
