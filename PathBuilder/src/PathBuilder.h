/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PLANT_FLAG_TOOL_H
#define PLANT_FLAG_TOOL_H

#include <rviz/tool.h>
#include <rviz/properties/float_property.h>
#include <rviz/frame_position_tracking_view_controller.h>
#include <QVector>

#include <ros/ros.h>

#include "pathbuilderpanel.h"
#include "Path.h"
#include <OGRE/OgreRay.h>

namespace Ogre
{
class SceneNode;
class Vector3;
}

namespace rviz
{
class VectorProperty;
class VisualizationManager;
class ViewportMouseEvent;
class FloatProperty;
}

namespace Gende
{
enum MouseState {
    Free=0,
    CreatePath=1,
    SelectEnity=2
};
// BEGIN_TUTORIAL
// Here we declare our new subclass of rviz::Tool.  Every tool
// which can be added to the tool bar is a subclass of
// rviz::Tool.
class PathBuilder: public rviz::Tool
{
Q_OBJECT
public:
  PathBuilder();
  ~PathBuilder();

  virtual void onInitialize();

  virtual void activate();
  virtual void deactivate();

  virtual int processMouseEvent( rviz::ViewportMouseEvent& event );
  virtual int processKeyEvent( QKeyEvent* event, RenderPanel* panel );

  virtual void load( const rviz::Config& config );
  virtual void save( rviz::Config config ) const;
public Q_SLOTS:
  void createPath(); // create a new path
  void pathSelectedInPanel(QString path_id);
  void publishPlan(QString path_id);
  void hightLightPath(QString path_id);
private:
  void makeFlag( const Ogre::Vector3& position );
  void zoom( float amount );
  void updateCamera();
  Path* makeNewPath(const Ogre::Vector3& position);
  void getVertexFromPath(Path* path);
  PathVertex* getPathVertexByName(const std::string& name);
  // insert more points on the path
  std::vector<Ogre::Vector3> makeMorePointsOnPath(std::vector<PathVertex*>& path_ori);

  std::string generatePathName();
  std::map<QString,Path*> pathPool;
  Path* currentActivePath;
  Ogre::Camera* camera_;
  rviz::FloatProperty* distance_property_;    ///< The camera's distance from the focal point

  ros::Publisher global_plan_pub_; // publish "global_plan" nav_msgs::Path
  ros::Publisher goal_pub_;  // publish "move_base_simple/goal" geometry_msgs/PoseStamped

  PathBuilderPanel *panel;
  int pointCount;
  std::vector<Ogre::Vector3> myPath;
  Ogre::RaySceneQuery* mRayScnQuery;
  QVector<Ogre::Entity *> hightLightEntityList;
  MouseState currentMouseState;
  Ogre::Entity *currentSelectEntity;
  std::map<std::string,PathVertex*> pathVertexPool;
  std::map<std::string,PathLine*> PathLinePool;

  std::vector<Ogre::SceneNode*> flag_nodes_;
  Ogre::SceneNode* moving_flag_node_;
  std::string flag_resource_;
  rviz::VectorProperty* current_flag_property_;
};
// END_TUTORIAL

} // end namespace Gende

#endif // PLANT_FLAG_TOOL_H
