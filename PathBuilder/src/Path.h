#ifndef PATH_H
#define PATH_H

#include <rviz/ogre_helpers/object.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreMaterial.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreManualObject.h>
#include <OGRE/OgreEntity.h>

#include <QObject>

#include "pathvertex.h"
#include "pathline.h"

using namespace rviz;



namespace Gende
{
class SceneManager;
class SceneNode;
class Vector3;
class Quaternion;
class Any;
class ColourValue;
class PathVertex;
class PathLine;

const int MAGIC_NUMBER = 1000;

/// Item Types
enum ITEM_TYPE{
    PATH_ITEM_TYPE = MAGIC_NUMBER,
    VERTEX_ITEM_TYPE = MAGIC_NUMBER+1,
    EDGE_ITEM_TYPE = MAGIC_NUMBER+2
};

class Path: public QObject,public Object
{
Q_OBJECT
public:
    Path(Ogre::SceneManager* manager, Ogre::SceneNode* parent_node,
         std::string &name,
         const Ogre::Vector3& position,
         std::string meshName);

    /**
     * \brief Set the position of this object
     * @param Position vector position to set to.
     */
    virtual void setPosition( const Ogre::Vector3& position );

    /**
     * \brief Set the orientation of the object
     * @param Orientation quaternion orientation to set to.
     */
    virtual void setOrientation( const Ogre::Quaternion& orientation );

    /**
     * \brief Set the scale of the object.  Always relative to the identity orientation of the object.
     * @param Scale vector scale to set to.
     */
    virtual void setScale( const Ogre::Vector3& scale );

    /**
     * \brief Set the color of the object.  Values are in the range [0, 1]
     * @param r Red component
     * @param g Green component
     * @param b Blue component
     */
    virtual void setColor( float r, float g, float b, float a );

    /**
     * \brief Get the local position of this object
     * @return The position
     */
    virtual const Ogre::Vector3& getPosition();
    /**
     * \brief Get the local orientation of this object
     * @return The orientation
     */
    virtual const Ogre::Quaternion& getOrientation();

    /**
     * \brief Set the user data on this object
     * @param data
     */
    virtual void setUserData( const Ogre::Any& data );

    void emit_create_info();
    void appendVertex(const Ogre::Vector3& position);
    //TODO: connect, disconnect path, delete vertex
    std::string generateVertexName();

    std::vector<PathVertex*>& getVertexPool() { return vertexPool; }
    std::vector<PathLine*>& getLinePool() { return linePool; }

    // name=id
    std::string getPathName() { return path_name; }
    // get index of the pv
    int getVertexIndex(PathVertex *pv);
Q_SIGNALS:
    void emit_path_info(QString);
    void emit_vertex_info(PathVertex* pv);
private:
    std::vector<PathVertex*> vertexPool;
    std::vector<PathLine*> linePool;
    std::string path_name;
    std::string meshName_;
protected:

    Ogre::SceneManager* manager_;
    Ogre::SceneNode* scene_node_;
    Ogre::ManualObject* manual_object_;
    Ogre::MaterialPtr manual_object_material_;
};

}// end namespace Gende
#endif // PATH_H
