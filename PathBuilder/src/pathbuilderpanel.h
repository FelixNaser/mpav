#ifndef PATHBUILDERPANEL_H
#define PATHBUILDERPANEL_H

#include <QWidget>
#include <QTreeWidget>
#include <QPushButton>

#include "Path.h"
#include "pathvertex.h"

namespace Gende
{

class PathBuilderPanel : public QWidget
{
    Q_OBJECT

public:
    PathBuilderPanel(QWidget* parent = 0);
public Q_SLOTS:
    void createPathClicked();
    void addPathItem(QString pathId);
    void addVertexItem(PathVertex *pv);
    void updateVertexPosition(PathVertex *pv);
    void doItemClicked(QTreeWidgetItem* item, int column);
    void publishPathClicked();
Q_SIGNALS:
    void sendCreatePathState();
    void emit_path_selected(QString);
    void emit_publish_path(QString);

private:
    QTreeWidget *itemsWidget;
    QPushButton *button_publish_path;
    QString current_selected_path;
protected:
    void mousePressEvent(QMouseEvent *event);
};

} //end namespace Gende
#endif // PATHBUILDERPANEL_H
