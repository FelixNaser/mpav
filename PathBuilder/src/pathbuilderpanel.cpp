#include "pathbuilderpanel.h"

#include <QHBoxLayout>
#include <QHeaderView>

#include <ros/console.h>

using namespace Gende;

Gende::PathBuilderPanel::PathBuilderPanel(QWidget* parent)
    : QWidget(parent)
{
    this->resize(200,200);
//    QObject::connect(this,SIGNAL(QWidget::mousePressEvent(QMouseEvent *)),this,SLOT( panelClicked(QMouseEvent *) ) );
    //
    QPushButton *button = new QPushButton("&CreateP", this);
    QObject::connect(button,SIGNAL(clicked()),this,SLOT( createPathClicked() ) );
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(button);
    this->setLayout(layout);
    //
    //publish path button
    button_publish_path = new QPushButton("&PubP", this);
    QObject::connect(button_publish_path,SIGNAL(clicked()),this,SLOT( publishPathClicked() ) );
    // only path selected ,enable the button
    button_publish_path->setDisabled(true);
    layout->addWidget(button_publish_path);
    //
    itemsWidget = new QTreeWidget(this);
    //for path row: path id, ,
    //for vertex row: index,position,id
    itemsWidget->setColumnCount(3);
    layout->addWidget(itemsWidget);
    QStringList ITEM_HEADERS = QStringList() << "Path" << "Value"<<"Id";
    itemsWidget->setHeaderLabels(ITEM_HEADERS);
    (itemsWidget->header())->setResizeMode(QHeaderView::ResizeToContents);
    connect(itemsWidget, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(doItemClicked(QTreeWidgetItem*, int)));
    // test tree widget
    //QTreeWidgetItem *item = new QTreeWidgetItem(itemsWidget, QStringList(QString("item: %1")));
    //itemsWidget->insertTopLevelItem(0, item);

//    this->show();  // set show or not in PathBuilder
    current_selected_path="";
    ROS_INFO("PathBuilderPanel created successfully");
}
void Gende::PathBuilderPanel::createPathClicked()
{
    Q_EMIT( sendCreatePathState() );
}
void Gende::PathBuilderPanel::addPathItem(QString pathId)
{
    ROS_INFO("addPathItem");
    QList<QTreeWidgetItem *> itemList = itemsWidget->findItems(pathId,Qt::MatchContains , 0);
    if(itemList.size()>0)
    {
        // already created
        return;
    }
    QStringList str = QStringList()<<pathId<<""<<pathId;
    QTreeWidgetItem *item = new QTreeWidgetItem(itemsWidget,str,Gende::PATH_ITEM_TYPE);
    // path is first level item
    itemsWidget->insertTopLevelItem(0,item);
    itemsWidget->update();
}
void Gende::PathBuilderPanel::addVertexItem(PathVertex *pv)
{
    ROS_INFO("addVertexItem");
    std::string pathId = pv->getParentPath()->getPathName();
    std::string vertexId = pv->getName();
    int idx = pv->getParentPath()->getVertexIndex(pv);
    //find path item
    QList<QTreeWidgetItem *> itemList =
            itemsWidget->findItems(QString().fromStdString(pathId),
                                   Qt::MatchContains|Qt::MatchRecursive ,
                                   0);
    if(itemList.size()==0)
    {
        ROS_INFO("error not find path %s",pathId.c_str());
        return;
    }
    QTreeWidgetItem* pathItem = itemList.at(0);
    // add vertex as child to path item
    QTreeWidgetItem *vertex_item = new QTreeWidgetItem(pathItem,Gende::VERTEX_ITEM_TYPE);
    vertex_item->setText(0,QString("%1").arg(idx));
    vertex_item->setText(1,QString("%1 %2").arg(pv->getPosition().x).arg(pv->getPosition().y));
    vertex_item->setText(2,QString().fromStdString(vertexId));
    pathItem->addChild(vertex_item);
//    // add position as child of vertex
//    QTreeWidgetItem *item_pos = new QTreeWidgetItem(vertex_item);
//    item_pos->setText(0,QString("position"));
//    item_pos->setText(1,QString("%1 %2").arg(pv->getPosition().x).arg(pv->getPosition().y));
    //


    itemsWidget->update();
}
void Gende::PathBuilderPanel::updateVertexPosition(PathVertex *pv)
{
    // find vertex item in column 2
    QList<QTreeWidgetItem *> itemList =
            itemsWidget->findItems(QString().fromStdString(pv->getName()),
                                   Qt::MatchContains|Qt::MatchRecursive ,
                                   2);
    if(itemList.size()==0)
    {
        ROS_INFO("error not find vertex %s",pv->getName().c_str());
        return;
    }
    QTreeWidgetItem* vertex_item = itemList.at(0);
    vertex_item->setText(1,QString("%1 %2").arg(pv->getPosition().x).arg(pv->getPosition().y));

    itemsWidget->update();
}
void Gende::PathBuilderPanel::doItemClicked(QTreeWidgetItem *item, int column)
{
    if(item->type() == Gende::PATH_ITEM_TYPE)
    {
        QString path_id = item->text(2);
        Q_EMIT emit_path_selected(path_id);
        button_publish_path->setEnabled(true);
        current_selected_path = path_id;
    }
    else
    {
        button_publish_path->setDisabled(true);
    }
    //TODO if vertex ,edge
}
void Gende::PathBuilderPanel::publishPathClicked()
{
    if(!current_selected_path.isEmpty())
    {
        Q_EMIT emit_publish_path(current_selected_path);
    }
}
void Gende::PathBuilderPanel::mousePressEvent(QMouseEvent *event)
{
//    ROS_INFO("panel clicked");
    current_selected_path = "";
    itemsWidget->clearSelection();
    button_publish_path->setDisabled(true);
}
