/*
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreManualObject.h>
#include <OGRE/OgreEntity.h>

#include <ros/console.h>
#include <nav_msgs/Path.h>
#include <tf/transform_datatypes.h>

#include <rviz/viewport_mouse_event.h>
#include <rviz/visualization_manager.h>
#include <rviz/mesh_loader.h>
#include <rviz/geometry.h>
#include <rviz/properties/vector_property.h>
#include <rviz/ogre_helpers/line.h>
#include <rviz/render_panel.h>

#include "PathBuilder.h"


using namespace Gende;


// BEGIN_TUTORIAL
// Construction and destruction
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
// The constructor must have no arguments, so we can't give the
// constructor the parameters it needs to fully initialize.
//
// Here we set the "shortcut_key_" member variable defined in the
// superclass to declare which key will activate the tool.
PathBuilder::PathBuilder()
  : moving_flag_node_( NULL )
  , current_flag_property_( NULL )
  , camera_( NULL )
{
  shortcut_key_ = 'l';
  //
  float DISTANCE_START = 10;
  distance_property_ = new rviz::FloatProperty( "Distance", DISTANCE_START, "Distance from the focal point." );
  distance_property_->setMin( 0.01 );
}

// The destructor destroys the Ogre scene nodes for the flags so they
// disappear from the 3D scene.  The destructor for a Tool subclass is
// only called when the tool is removed from the toolbar with the "-"
// button.
PathBuilder::~PathBuilder()
{
  for( unsigned i = 0; i < flag_nodes_.size(); i++ )
  {
    scene_manager_->destroySceneNode( flag_nodes_[ i ]);
  }
  // destroy panel
  if(panel)
  {
      delete panel;
  }
  //delete all paths

}

// onInitialize() is called by the superclass after scene_manager_ and
// context_ are set.  It should be called only once per instantiation.
// This is where most one-time initialization work should be done.
// onInitialize() is called during initial instantiation of the tool
// object.  At this point the tool has not been activated yet, so any
// scene objects created should be invisible or disconnected from the
// scene at this point.
//
// In this case we load a mesh object with the shape and appearance of
// the flag, create an Ogre::SceneNode for the moving flag, and then
// set it invisible.
void PathBuilder::onInitialize()
{
    ros::NodeHandle global_node;
    global_plan_pub_ = global_node.advertise<nav_msgs::Path>("route_plan", 1, true);
    goal_pub_ = global_node.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal",1,true);
    //
    flag_resource_ = "package://PathBuilder/media/flag.dae";

    if( rviz::loadMeshFromResource( flag_resource_ ).isNull() )
    {
    ROS_ERROR( "PathBuilder: failed to load model resource '%s'.", flag_resource_.c_str() );
    return;
    }

    moving_flag_node_ = scene_manager_->getRootSceneNode()->createChildSceneNode();
    Ogre::Entity* entity = scene_manager_->createEntity("PB_mouse", flag_resource_ );
    moving_flag_node_->attachObject( entity );
    moving_flag_node_->setVisible( false );
    //
    panel = new PathBuilderPanel();
    panel->setVisible(false);
    QObject::connect(panel,SIGNAL(sendCreatePathState()),this,SLOT(createPath()));
    QObject::connect(panel,SIGNAL(emit_path_selected(QString)),this,SLOT(pathSelectedInPanel(QString)));
    QObject::connect(panel,SIGNAL(emit_publish_path(QString)),this,SLOT(publishPlan(QString)));
    //
    pointCount=0;
    mRayScnQuery = scene_manager_->createRayQuery(Ogre::Ray());
    currentActivePath = NULL;

    // init camera
    camera_ = context_->getSceneManager()->createCamera( "PathBuilder" );
    camera_->setProjectionType( Ogre::PT_PERSPECTIVE );  // what means?
}
void PathBuilder::pathSelectedInPanel(QString path_id)
{
    ROS_INFO("path selected %s",path_id.toStdString().c_str());
    hightLightPath(path_id);
}
void PathBuilder::hightLightPath(QString path_id)
{
    std::map<QString,Path*>::iterator it = pathPool.find(path_id);
    if(it!=pathPool.end())
    {
        Path* p = it->second;
        std::vector<PathVertex*> path_vertex = p->getVertexPool();
        for(int i=0;i<path_vertex.size();++i)
        {
            PathVertex* pv = path_vertex[i];
            pv->getEntity()->getParentSceneNode()->showBoundingBox(true);
            hightLightEntityList.push_back(pv->getEntity());
        }
    }
}
std::vector<Ogre::Vector3> PathBuilder::makeMorePointsOnPath(std::vector<PathVertex*>& path_ori)
{
    ROS_INFO("makeMorePointsOnPath ori path has points: %d",path_ori.size());
    double point_intervel = 0.1;
    std::vector<Ogre::Vector3> path_res;
    for(unsigned int i=0; i<path_ori.size(); i++)
    {
        ROS_INFO("points: %d",i);
        if(i==0)
        {
            // always store original points
            path_res.push_back(path_ori[i]->getPosition());
            continue;
        }
        bool isStillCanInsert = true;
        Ogre::Vector3 current_point = path_ori[i]->getPosition();
        while(isStillCanInsert)
        {
            Ogre::Vector3 last_point = path_res.back();
            double dy = current_point.y - last_point.y;
            double dx = current_point.x - last_point.x;
            double dis = sqrt(dy*dy + dx*dx);
            if(dis > point_intervel)
            {
                ROS_INFO("dis: %f",dis);
                // still has space to insert point
                // 1.0 make new point
                Ogre::Vector3 newPoint;
                // 1.1 computer k
                double k = atan2(dy,dx);
                newPoint.x = last_point.x + point_intervel*cos(k);
                newPoint.y = last_point.y + point_intervel*sin(k);
                // 2.0 insert new point to path_res
                path_res.push_back(newPoint);
            }
            else
            {
                isStillCanInsert = false;
            }
        }//while

        path_res.push_back(path_ori[i]->getPosition());

    }//for

    return path_res;
}

void PathBuilder::publishPlan(QString path_id)
{
    std::map<QString,Path*>::iterator it = pathPool.find(path_id);
    if(it == pathPool.end())
    {
        ROS_INFO("publish path %s no found",path_id.toStdString().c_str());
        return;
    }
    Path* path = it->second;
    std::vector<PathVertex*> path_vertexs = path->getVertexPool();
    //do insert
    std::vector<Ogre::Vector3> path2 = makeMorePointsOnPath(path_vertexs);
    ROS_INFO("path2 has points: %d",path2.size());
    nav_msgs::Path p;
    p.poses.resize(path2.size());
    for(int i=0;i<path2.size();++i)
    {
        Ogre::Vector3 path_position = path2[i];
        p.poses[i].header.frame_id = "/map";
        p.poses[i].header.stamp = ros::Time::now();
        p.poses[i].pose.position.x = path_position.x;
        p.poses[i].pose.position.y = path_position.y;
        p.poses[i].pose.orientation.w = 1.0;
    }
//    p.poses.resize(path_vertexs.size());
//    for(unsigned int i=0; i<path_vertexs.size(); i++)
//    {
//        const Ogre::Vector3 path_position = path_vertexs[i]->getPosition();
//        p.poses[i].header.frame_id = "/map";
//        p.poses[i].header.stamp = ros::Time::now();
//        p.poses[i].pose.position.x = path_position.x;
//        p.poses[i].pose.position.y = path_position.y;
//        p.poses[i].pose.orientation.w = 1.0;
//    }
    ROS_INFO("publish plan with %d points sent.",p.poses.size() );
    p.header.stamp = ros::Time();
    p.header.frame_id = "/map";

    global_plan_pub_.publish(p);

    // publish goal
    sleep(1);
    geometry_msgs::PoseStamped goal;
    // header
//    goal.header.seq =1;
//    goal.header.frame_id = "/map";
    // goal position
    const Ogre::Vector3 path_last_point = path_vertexs.back()->getPosition();
//    goal.pose.position.x = path_last_point.x;
//    goal.pose.position.y = path_last_point.y;
    // goal orietation
    double yaw=0;
    if(path_vertexs.size() == 1)
    {

        // as only one point in whole path, no orientation info
        yaw=0; // theta
//        goal.pose.orientation = tf::createQuaternionFromRPY(roll, pitch, yaw);
    }
    else if(path_vertexs.size()>1)
    {
        const Ogre::Vector3 path_position = path_vertexs[path_vertexs.size()-2]->getPosition();
        double dy = path_last_point.y - path_position.y;
        double dx = path_last_point.x - path_position.x;
        yaw = atan2(dy,dx);
        ROS_INFO("goal  dy <%f> dx<%f> yaw<%f>",dy,dx,yaw*57.3);
//        goal.pose.orientation = tf::createQuaternionFromYaw(yaw);
    }
    tf::Quaternion quat;
    quat.setRPY(0.0, 0.0, yaw);
    tf::Stamped<tf::Pose> ps = tf::Stamped<tf::Pose>(tf::Pose(quat, tf::Point(path_last_point.x, path_last_point.y, 0.0)), ros::Time::now(), "/map");
    tf::poseStampedTFToMsg(ps, goal);
    goal_pub_.publish(goal);
}

// Activation and deactivation
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
// activate() is called when the tool is started by the user, either
// by clicking on its button in the toolbar or by pressing its hotkey.
//
// First we set the moving flag node to be visible, then we create an
// rviz::VectorProperty to show the user the position of the flag.
// Unlike rviz::Display, rviz::Tool is not a subclass of
// rviz::Property, so when we want to add a tool property we need to
// get the parent container with getPropertyContainer() and add it to
// that.
//
// We wouldn't have to set current_flag_property_ to be read-only, but
// if it were writable the flag should really change position when the
// user edits the property.  This is a fine idea, and is possible, but
// is left as an exercise for the reader.
void PathBuilder::activate()
{
    ROS_INFO("activate");
    if(panel)
    {
        panel->setVisible(true);
    }
    currentMouseState = Gende::Free;
    currentSelectEntity = NULL;
//  if( moving_flag_node_ )
//  {
//    moving_flag_node_->setVisible( true );

//    current_flag_property_ = new rviz::VectorProperty( "Flag " + QString::number( flag_nodes_.size() ));
//    current_flag_property_->setReadOnly( true );
//    getPropertyContainer()->addChild( current_flag_property_ );
//  }
}
void PathBuilder::createPath()
{
    currentMouseState = Gende::CreatePath;
    if( moving_flag_node_ )
    {
      moving_flag_node_->setVisible( true );

      current_flag_property_ = new rviz::VectorProperty( "Flag " + QString::number( flag_nodes_.size() ));
      current_flag_property_->setReadOnly( true );
      getPropertyContainer()->addChild( current_flag_property_ );
    }
     ROS_INFO("createPath");
}

// deactivate() is called when the tool is being turned off because
// another tool has been chosen.
//
// We make the moving flag invisible, then delete the current flag
// property.  Deleting a property also removes it from its parent
// property, so that doesn't need to be done in a separate step.  If
// we didn't delete it here, it would stay in the list of flags when
// we switch to another tool.
void PathBuilder::deactivate()
{
  // why delete?
  if( moving_flag_node_ )
  {
    moving_flag_node_->setVisible( false );
    delete current_flag_property_;
    current_flag_property_ = NULL;
  }
  if(panel)
  {
      panel->setVisible(false);
  }
  //
  currentActivePath = NULL;
}

// Handling mouse events
// ^^^^^^^^^^^^^^^^^^^^^
//
// processMouseEvent() is sort of the main function of a Tool, because
// mouse interactions are the point of Tools.
//
// We use the utility function rviz::getPointOnPlaneFromWindowXY() to
// see where on the ground plane the user's mouse is pointing, then
// move the moving flag to that point and update the VectorProperty.
//
// If this mouse event was a left button press, we want to save the
// current flag location.  Therefore we make a new flag at the same
// place and drop the pointer to the VectorProperty.  Dropping the
// pointer means when the tool is deactivated the VectorProperty won't
// be deleted, which is what we want.
int PathBuilder::processMouseEvent( rviz::ViewportMouseEvent& event )
{
//  ROS_INFO("x:%d y:%d",event.x,event.y);
  if( !moving_flag_node_ )
  {
    return Render;
  }
  if( event.rightDown() )
  {
      // mouse right button down, go back to Free mode
      currentMouseState = Gende::Free;
      currentActivePath = NULL;
      // clear hight light list, so entity not always hight light
      for(int i=0;i<hightLightEntityList.size();++i)
      {
          hightLightEntityList[i]->getParentSceneNode()->showBoundingBox(false);
      }
      hightLightEntityList.clear();
      return Render;
  }
  //
  Ogre::Vector3 intersection;
  Ogre::Plane ground_plane( Ogre::Vector3::UNIT_Z, 0.0f );
  if(currentMouseState == Gende::CreatePath)
  {
      if( rviz::getPointOnPlaneFromWindowXY( event.viewport,
                                             ground_plane,
                                             event.x, event.y, intersection ))
      {
        moving_flag_node_->setVisible( true );
        moving_flag_node_->setPosition( intersection );
        current_flag_property_->setVector( intersection );

        if( event.leftDown() )
        {
          ROS_INFO("x:%d y:%d",event.x,event.y);
//          makeFlag( intersection );
            if(!currentActivePath)
            {
                currentActivePath = makeNewPath(intersection);
                getVertexFromPath(currentActivePath);
            }
            else
            {
                currentActivePath->appendVertex(intersection);
                getVertexFromPath(currentActivePath);
            }
//          ROS_INFO("createPath2.0");
          // do not set null
//          current_flag_property_ = NULL; // Drop the reference so that deactivate() won't remove it.
//          return Render;
        }
      } // end if getPointOnPlaneFromWindowXY
  }
  else if(currentMouseState == Gende::Free)
  {
    //  Ogre::Entity* mEntity = static_cast<Ogre::Entity*>(moving_flag_node_->getAttachedObject(0));
    //  moving_flag_node_->showBoundingBox(true);

//      // clear hight light list, so entity not always hight light
//      for(int i=0;i<hightLightEntityList.size();++i)
//      {
//          hightLightEntityList[i]->getParentSceneNode()->showBoundingBox(false);
//      }
//      hightLightEntityList.clear();
      // select object
      int width = event.viewport->getActualWidth();
      int height = event.viewport->getActualHeight();
      Ogre::Ray mouse_ray = event.viewport->getCamera()->getCameraToViewportRay( event.x/ (float)width,
                                                                           event.y / (float)height );
      mRayScnQuery->setRay(mouse_ray);
      Ogre::RaySceneQueryResult& result = mRayScnQuery->execute();
      Ogre::RaySceneQueryResult::iterator itr;
      Ogre::Entity *entity = NULL;
      for ( itr = result.begin( ); itr != result.end(); itr++ )
      {
          if ( itr->movable && itr->movable->getName().find("PB_") != std::string::npos
               && itr->movable->getName().find("__") == std::string::npos) // filter pathline
        {
              ROS_DEBUG("find movable PB object %s",itr->movable->getName().c_str());
            entity = (Ogre::Entity *)itr->movable;
            entity->getParentSceneNode()->showBoundingBox(true);
            hightLightEntityList.push_back(entity);
        }
      }
      // mouse point to a object and left down, it means select this object
      if( event.leftDown() && entity)
      {
          if( rviz::getPointOnPlaneFromWindowXY( event.viewport,
                                                 ground_plane,
                                                 event.x, event.y, intersection ))
          {
            currentSelectEntity = entity;
            currentMouseState = Gende::SelectEnity;
          }
      }// end leftdown
  }// end Free
  else if( currentMouseState == Gende::SelectEnity )
  {
      if(!currentSelectEntity)
      {
          ROS_INFO("error, in Gende::SelectEnity state ,but no entity");
      }
      // drag entity
      if( event.left() )
      {
          if( rviz::getPointOnPlaneFromWindowXY( event.viewport,
                                                 ground_plane,
                                                 event.x, event.y, intersection ))
          {
              PathVertex *pv = getPathVertexByName(currentSelectEntity->getName());
              if(pv)
              {
                  pv->setPosition(intersection);
                  panel->updateVertexPosition(pv);
              }
          }
      }
      // drop the object
      if( event.leftUp() )
      {
        currentMouseState = Gende::Free;
        currentSelectEntity = NULL;
      }
  }// end SelectEntity

  // use viewcontroller to handle mouse wheel and middle click
  if( event.wheel_delta != 0 || event.middle() )
  {
//      ROS_INFO("wheel_delta %d",event.wheel_delta);
//      float distance = distance_property_->getFloat();
//      int diff = event.wheel_delta;
//      //TODO: shift
//      zoom( diff * 0.001 * distance );

      //
      if ( event.panel->getViewController() )
      {
          event.panel->getViewController()->handleMouseEvent(event);
      }
//      Ogre::Camera* c = event.viewport->getCamera();
//      if(c)
//      {
//          ROS_INFO("we get Camera!");
//          c->setPosition( c->getPosition() + c->getUp() * distance_property_->getFloat() * 0.2 );
//          scene_manager_->_renderScene(c,event.viewport,true);
//      }

      context_->queueRender();
  }//end wheel
  return Render;
}
int Gende::PathBuilder::processKeyEvent(QKeyEvent *event, RenderPanel *panel)
{
     if(event->key() == Qt::Key_Delete)
     {
         ROS_INFO("delete key pressed");
     }
     return Render;
}

void Gende::PathBuilder::zoom( float amount )
{
  distance_property_->add( -amount );
}
void Gende::PathBuilder::updateCamera()
{
//  float distance = distance_property_->getFloat();
//    camera_->getOrientation();

//  float x = distance * cos( yaw ) * cos( pitch ) + focal_point.x;
//  float y = distance * sin( yaw ) * cos( pitch ) + focal_point.y;
//  float z = distance *              sin( pitch ) + focal_point.z;

//  Ogre::Vector3 pos( x, y, z );

//  camera_->setPosition(pos);
}
Path* Gende::PathBuilder::makeNewPath(const Ogre::Vector3& position)
{
    //
    std::string path_name = generatePathName();
    Gende::Path* newPath = new Gende::Path(scene_manager_,
                                           scene_manager_->getRootSceneNode(),
                                           path_name,
                                           position,flag_resource_);
    pathPool.insert(std::make_pair(QString().fromStdString(path_name), newPath));
    //
    qRegisterMetaType<PathVertex*>("PathVertex*");
    QObject::connect(newPath,SIGNAL(emit_path_info(QString)),panel,SLOT(addPathItem(QString)),Qt::QueuedConnection);
    QObject::connect(newPath,SIGNAL(emit_vertex_info(PathVertex*)),panel,SLOT(addVertexItem(PathVertex*)),Qt::QueuedConnection);
    newPath->emit_create_info();
    return newPath;
}
void Gende::PathBuilder::getVertexFromPath(Path *path)
{
    std::vector<PathVertex*> vPool = path->getVertexPool();
    for(int i=0;i<vPool.size();++i)
    {
        PathVertex* pv = vPool[i];
        std::string name = pv->getName();
        pathVertexPool.insert(std::make_pair(name,pv));
    }
    //TODO: line pool
}
PathVertex* Gende::PathBuilder::getPathVertexByName(const std::string &name)
{
    std::map<std::string,Gende::PathVertex*>::iterator it = pathVertexPool.find(name);
    if(it==pathVertexPool.end())
    {
        //no found
        ROS_INFO("getPathVertexByName: no found %s",name.c_str());
    }
    return it->second;
}

std::string Gende::PathBuilder::generatePathName()
{
    char name[16];
    sprintf(name, "PB_Path_%d", pathPool.size()+1);
    std::string path_name( name );
    return path_name;
}

// This is a helper function to create a new flag in the Ogre scene and save its scene node in a list.
void PathBuilder::makeFlag( const Ogre::Vector3& position )
{
  Ogre::SceneNode* node = scene_manager_->getRootSceneNode()->createChildSceneNode();
  char name[16];
  sprintf(name, "PB_point_%d", myPath.size()+1);
  std::string entity_name( name );
  Ogre::Entity* entity = scene_manager_->createEntity(entity_name, flag_resource_ );
  node->attachObject( entity );
  node->setVisible( true );
  node->setPosition( position );
  flag_nodes_.push_back( node );

  // test line
  Ogre::Vector3 start_pos;
  Ogre::Vector3 end_pos;

  start_pos = position;
  end_pos.x = position.x;
  end_pos.y = position.y+1.0;
  end_pos.z = position.z;

//  rviz::Line *line_ = new rviz::Line(scene_manager_,scene_manager_->getRootSceneNode());
//  line_->setPoints(start_pos,end_pos);
//  line_->setVisible(true);
//  ROS_INFO("line created");

  // test Ogre::ManualObject
  Ogre::ManualObject* manual_object;
  if(scene_manager_->hasManualObject("myPath"))
  {
      manual_object = scene_manager_->getManualObject("myPath");
//      pointCount++;
      myPath.push_back(start_pos);
      manual_object->estimateVertexCount( myPath.size() );
      manual_object->beginUpdate(0);
      for(int i=0;i<myPath.size();++i)
      {
          manual_object->position(myPath[i]);
      }
      manual_object->end();
      ROS_INFO("manual object add vertex");
  }
  else
  {
      manual_object = scene_manager_->createManualObject("myPath");
      myPath.push_back(start_pos);
      manual_object->setDynamic( true );
      scene_manager_->getRootSceneNode()->attachObject( manual_object );
      manual_object->estimateVertexCount( myPath.size() );
      manual_object->begin( "BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP );
      manual_object->position( start_pos );
//      manual_object->position( end_pos.x+1.0, end_pos.y, end_pos.z );
      manual_object->end();

      ROS_INFO("manual object created");
  }


  // dont want suoxiao
//  Ogre::Vector3 scale_;
//  scale_.x = 0.2;
//  scale_.y = 0.2;
//  line_->setScale(scale_);
}

// Loading and saving the flags
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
// Tools with a fixed set of Property objects representing adjustable
// parameters are typically just created in the tool's constructor and
// added to the Property container (getPropertyContainer()).  In that
// case, the Tool subclass does not need to override load() and save()
// because the default behavior is to read all the Properties in the
// container from the Config object.
//
// Here however, we have a list of named flag positions of unknown
// length, so we need to implement save() and load() ourselves.
//
// We first save the class ID to the config object so the
// rviz::ToolManager will know what to instantiate when the config
// file is read back in.
void PathBuilder::save( rviz::Config config ) const
{
  config.mapSetValue( "Class", getClassId() );

  // The top level of this tool's Config is a map, but our flags
  // should go in a list, since they may or may not have unique keys.
  // Therefore we make a child of the map (``flags_config``) to store
  // the list.
  rviz::Config flags_config = config.mapMakeChild( "Flags" );

  // To read the positions and names of the flags, we loop over the
  // the children of our Property container:
  rviz::Property* container = getPropertyContainer();
  int num_children = container->numChildren();
  for( int i = 0; i < num_children; i++ )
  {
    rviz::Property* position_prop = container->childAt( i );
    // For each Property, we create a new Config object representing a
    // single flag and append it to the Config list.
    rviz::Config flag_config = flags_config.listAppendNew();
    // Into the flag's config we store its name:
    flag_config.mapSetValue( "Name", position_prop->getName() );
    // ... and its position.
    position_prop->save( flag_config );
  }
}

// In a tool's load() function, we don't need to read its class
// because that has already been read and used to instantiate the
// object before this can have been called.
void PathBuilder::load( const rviz::Config& config )
{
  // Here we get the "Flags" sub-config from the tool config and loop over its entries:
  rviz::Config flags_config = config.mapGetChild( "Flags" );
  int num_flags = flags_config.listLength();
  for( int i = 0; i < num_flags; i++ )
  {
    rviz::Config flag_config = flags_config.listChildAt( i );
    // At this point each ``flag_config`` represents a single flag.
    //
    // Here we provide a default name in case the name is not in the config file for some reason:
    QString name = "Flag " + QString::number( i + 1 );
    // Then we use the convenience function mapGetString() to read the
    // name from ``flag_config`` if it is there.  (If no "Name" entry
    // were present it would return false, but we don't care about
    // that because we have already set a default.)
    flag_config.mapGetString( "Name", &name );
    // Given the name we can create an rviz::VectorProperty to display the position:
    rviz::VectorProperty* prop = new rviz::VectorProperty( name );
    // Then we just tell the property to read its contents from the config, and we've read all the data.
    prop->load( flag_config );
    // We finish each flag by marking it read-only (as discussed
    // above), adding it to the property container, and finally making
    // an actual visible flag object in the 3D scene at the correct
    // position.
    prop->setReadOnly( true );
    getPropertyContainer()->addChild( prop );
    makeFlag( prop->getVector() );
  }
}

// End of .cpp file
// ^^^^^^^^^^^^^^^^
//
// At the end of every plugin class implementation, we end the
// namespace and then tell pluginlib about the class.  It is important
// to do this in global scope, outside our package's namespace.


#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(Gende::PathBuilder,rviz::Tool )
// END_TUTORIAL
