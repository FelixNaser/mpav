/*
 * MetricChecker.hpp
 *
 *  Created on: 30 Mar, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_HYBRIDASTAR_METRICCHECKER_HPP_
#define INCLUDE_HYBRIDASTAR_METRICCHECKER_HPP_

#include <float.h>
#include <stdlib.h>

#include <Metric_Map/MetricMapMsg.h>

//#define LAZYCHECK

using namespace std;
using namespace boost;

namespace MotionPlan {

	class MetricChecker {
	public:
		MetricChecker();
		virtual ~MetricChecker();

		shared_ptr<Metric_Map::MetricMapMsg> metric_map_;
		void updateMetricMap(const shared_ptr<Metric_Map::MetricMapMsg>& metricMapIn){metric_map_ = metricMapIn;metric_initialzied_ = true;};
		double getMetric(const vector<double>& stateIn) const;

	private:

		bool metric_initialzied_;
		vector<double> veh_model_;
		enum {Width, Height,DistRear,SafetyGap};

		double getClearance(const vector<double>& stateIn)const;
		int getStateIndex(const double& x, const double& y, int& indexOut)const;
		void transformToMetricMapFrame(const vector<double>& stateIn, vector<double>& poseOut) const;

	};


}  // namespace MotionPlan


#endif /* INCLUDE_HYBRIDASTAR_METRICCHECKER_HPP_ */
