/*
 * HybridAstarPlanner.cpp
 *
 *  Created on: 24 Mar, 2015
 *      Author: liuwlz
 */


#include <HybridAStar/HybridAstarPlanner.hpp>

namespace MotionPlan {
	HybridAStarPlanner::
	HybridAStarPlanner():
	pri_nh_("~"),
	planning_duration_(0.5),
	is_goal_set_(false),
	is_metric_map_set_(false){
		pri_nh_.param("base_frame",base_frame_,string("/robot_0/base_link"));
		pri_nh_.param("global_frame",global_frame_,string("map"));
		plan_timer_ = nh_.createTimer(ros::Duration(planning_duration_),&HybridAStarPlanner::planTimer, this);
		metric_map_sub_ = nh_.subscribe("/robot_0/metric_map",1, &HybridAStarPlanner::metricMapCallBack,this);
		rviz_goal_sub_ = nh_.subscribe("rrts_goal",1,&HybridAStarPlanner::rvizGoalCallBack, this);
		solution_path_pub_ = nh_.advertise<nav_msgs::Path>("astar_solution", 1);
		search_result_pub_ = nh_.advertise<visualization_msgs::Marker>("search_result",1);

		hybrid_astar_ = shared_ptr<HybridAStar>(new HybridAStar());
	}

	HybridAStarPlanner::
	~HybridAStarPlanner(){

	}

	void
	HybridAStarPlanner::
	metricMapCallBack(const Metric_Map::MetricMapMsgPtr& metricMapIn){
		metric_map_ = metricMapIn;
		transformToGlobalFrame(metricMapIn->info.origin ,metric_map_->info.origin);
		metric_map_->info.origin.position.z = getYawofPose(metric_map_->info.origin);
		is_metric_map_set_ = true;
	}

	void
	HybridAStarPlanner::
	rvizGoalCallBack(const geometry_msgs::PoseStamped& goalIn){
		vector<double> goal_tmp(3);
		goal_tmp[0] = goalIn.pose.position.x;
		goal_tmp[1] = goalIn.pose.position.y;
		goal_tmp[2] = getYawofPose(goalIn.pose);
		setPlanningGoal(goal_tmp);
	}

	void
	HybridAStarPlanner::
	setPlanningGoal(const vector<double>& goalIn){
		hybrid_astar_->resetGoal(goalIn);
		is_goal_set_ = true;
	}

	void
	HybridAStarPlanner::
	planTimer(const ros::TimerEvent& e){
		if (!(is_goal_set_ && is_metric_map_set_))
			return;
		if (!getVehiclePose()){
			ROS_WARN("Failed to get vehicle pose!");
			return;
		}
		hybrid_astar_->resetStart(vehicle_pose_);
		hybrid_astar_->resetMetricMap(metric_map_);
		vector<State> solution_path;
		vector<int> shift_control;
		if (hybrid_astar_->searchPath(planning_duration_/4.0*3.0, solution_path, shift_control)){
			analyzeSolutionPath(solution_path, shift_control);
		}
		else{
			ROS_WARN("Failed to find a path for current loop!");
		}
		visualizeSearchResult();
	}

	void
	HybridAStarPlanner::
	analyzeSolutionPath(vector<State>& solutionPathIn, vector<int>& shiftCtrIn){
		solution_path_.header.frame_id = global_frame_;
		solution_path_.header.stamp = ros::Time::now();
		solution_path_.poses.clear();
		for(vector<State>::iterator iter = solutionPathIn.begin();
				iter != solutionPathIn.end(); iter++){
			geometry_msgs::PoseStamped current_pose;
			current_pose.pose.position.x = (*iter)[0];
			current_pose.pose.position.y = (*iter)[1];
			current_pose.pose.position.z = shiftCtrIn[iter - solutionPathIn.begin()];
			current_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0,0.0,(*iter)[2]);
			solution_path_.poses.push_back(current_pose);
		}
		solution_path_pub_.publish(solution_path_);
	}

	bool
	HybridAStarPlanner::
	getVehiclePose(){
		geometry_msgs::PoseStamped poseLocal, poseGlobal;
		poseLocal.header.stamp = ros::Time();
		poseLocal.header.frame_id = base_frame_;
		poseLocal.pose.position.x = 0.0;
		poseLocal.pose.position.y = 0.0;
		poseLocal.pose.orientation.w = 1.0;
		poseGlobal.header.stamp = ros::Time();
		poseGlobal.header.frame_id = global_frame_;
		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPose(global_frame_, poseLocal, poseGlobal);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		vehicle_pose_.resize(3);
		vehicle_pose_[0] = poseGlobal.pose.position.x;
		vehicle_pose_[1] = poseGlobal.pose.position.y;
		vehicle_pose_[2] = getYawofPose(poseGlobal.pose);
		return true;
	}

	double
	HybridAStarPlanner::
	getYawofPose(const geometry_msgs::Pose& poseIn){
		double roll, pitch, yaw;
		tf::Quaternion quaternion;
		tf::quaternionMsgToTF(poseIn.orientation, quaternion);
		tf::Matrix3x3 rotation(quaternion);
		rotation.getRPY(roll, pitch, yaw);
		return yaw;
	}

	bool
	HybridAStarPlanner::
	transformToGlobalFrame(const geometry_msgs::Pose& poseIn, geometry_msgs::Pose& poseOut){
		geometry_msgs::PoseStamped poseStampIn, poseStampOut;
		poseStampIn.header.stamp = ros::Time();
		poseStampIn.header.frame_id = base_frame_;
		poseStampIn.pose = poseIn;
		poseStampOut.header.stamp = ros::Time();
		poseStampOut.header.frame_id = global_frame_;

		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPose(global_frame_, poseStampIn, poseStampOut);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		poseOut = poseStampOut.pose;

		return true;
	}

	void
	HybridAStarPlanner::
	visualizeSearchResult(){
		visualization_msgs::Marker search_result_marker;
		search_result_marker.header.frame_id = global_frame_;
		search_result_marker.header.stamp = ros::Time::now();
		search_result_marker.action = visualization_msgs::Marker::ADD;
		search_result_marker.type = visualization_msgs::Marker::LINE_LIST;
		search_result_marker.ns = "Search_Result";
		search_result_marker.lifetime = ros::Duration(0.0);
		search_result_marker.scale.x = 0.01; search_result_marker.scale.y = 1.0; search_result_marker.scale.z = 1.0;
		search_result_marker.color.r = 0.0; search_result_marker.color.g = 1.0;
		search_result_marker.color.b = 0.0; search_result_marker.color.a = 0.8;
		for (vector<PathSegment>::iterator iter = hybrid_astar_->segments.begin();
				iter != hybrid_astar_->segments.end();
				iter ++){
			if (iter->prev_index == -1)
				continue;
			geometry_msgs::Point pts_tmp_child, pts_tmp_parent;
			pts_tmp_child.x = iter->state[0];
			pts_tmp_child.y = iter->state[1];
			pts_tmp_child.z = iter->state[2];
			search_result_marker.points.push_back(pts_tmp_child);
			pts_tmp_parent.x = hybrid_astar_->segments[iter->prev_index].state[0];
			pts_tmp_parent.y = hybrid_astar_->segments[iter->prev_index].state[1];
			pts_tmp_parent.z = hybrid_astar_->segments[iter->prev_index].state[2];
			search_result_marker.points.push_back(pts_tmp_parent);
		}
		cout <<"Marker pts size: "<<search_result_marker.points.size()<<endl;
		search_result_pub_.publish(search_result_marker);
	}

}  // namespace MotionPlan

