/*
 * main.cpp
 *
 *  Created on: 30 Mar, 2015
 *      Author: liuwlz
 */


#include <HybridAStar/HybridAstarPlanner.hpp>

int main(int argc, char**argv){
	ros::init(argc,argv,"hybrid_astar");
	MotionPlan::HybridAStarPlanner planner;
	ros::spin();
	return 1;
}

