/*
 * Speed_Smoother.cpp
 *
 *  Created on: 13 Aug, 2014
 *      Author: liuwlz
 */


#include <Speed_Smoother/Speed_Smoother.hpp>

namespace Control {
	SpeedSmoother::
	SpeedSmoother():
	pri_nh_("~"),
	odom_filter_time_(ros::Time::now()),
	auto_mode_(true),
	current_vel_(0.0),
	vel_callback_timeout_(0.5){

		vel_filter_ = shared_ptr<Util::LowPassFilter>(new Util::LowPassFilter(0.1));

		pri_nh_.param("max_acc",max_acc_,0.6);
		pri_nh_.param("max_dec",max_dec_,0.6);

		auto_mode_sub_ = nh_.subscribe("button_state_automode", 1, &SpeedSmoother::autoModeCallBack,this);
		odom_sub_ = nh_.subscribe("odom",1,&SpeedSmoother::odomCallBack,this);
		cmd_steer_sub_ = nh_.subscribe("cmd_steer",1,&SpeedSmoother::cmdSteerCallBack,this);
		cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel",1);
		speed_smooth_timer_ = nh_.createTimer(ros::Duration(0.02),&SpeedSmoother::speedSmoothTimer,this);
	}

	SpeedSmoother::
	~SpeedSmoother(){

	}

	void
	SpeedSmoother::
	autoModeCallBack(const std_msgs::Bool autoModeIn){
		auto_mode_ = autoModeIn.data;
	}

	void
	SpeedSmoother::
	odomCallBack(const nav_msgs::OdometryPtr odomIn){
		ros::Time time_now = ros::Time::now();
		current_vel_ = vel_filter_->filter_dt((time_now - odom_filter_time_).toSec(), odomIn->twist.twist.linear.x);
		odom_filter_time_ = time_now;
	}

	void
	SpeedSmoother::
	cmdSteerCallBack(const geometry_msgs::TwistPtr cmdSteerIn){
		cmd_vel_ = *cmdSteerIn;
		cmd_steer_filter_time_ = ros::Time::now();
	}

	void
	SpeedSmoother::
	speedSmooth(double speedIn, double& smoothedSpeed){
		if (SmootherDebug){
			ROS_INFO("Current Vel: %f, CMD_Vel: %f, smoothed_vel; %f", current_vel_, speedIn, smoothedSpeed);
		}
		//For Emergency stop case
		if (fabs(speedIn) < 1e-5){
			ROS_WARN("Receive Emergency Speed Command, stop immediately!");
			smoothedSpeed = speedIn;
			return;
		}
		if (speedIn > current_vel_ + max_acc_)
			smoothedSpeed = current_vel_ + max_acc_;
		else if (speedIn < current_vel_ - max_dec_)
			smoothedSpeed = current_vel_ - max_dec_;
		else
			smoothedSpeed = speedIn;
	}

	void
	SpeedSmoother::
	speedSmoothTimer(const ros::TimerEvent& e){
		if (!auto_mode_){
			cmd_vel_.linear.x = 0.0;
		}
		else{
			double smooth_speed_in = cmd_vel_.linear.x;
			if ((ros::Time::now()-odom_filter_time_).toSec() < vel_callback_timeout_ &&
					(ros::Time::now()-cmd_steer_filter_time_).toSec() < vel_callback_timeout_){
				speedSmooth(smooth_speed_in, cmd_vel_.linear.x);
			}
			else{
				ROS_INFO_ONCE("Velolicy Callback Timout, stop for safety");
				speedSmooth(1e-4, cmd_vel_.linear.x);
				cmd_vel_.angular.z = 0.0;
			}
		}
		cmd_vel_pub_.publish(cmd_vel_);
	}
}  // namespace Control

