/* Copyright (C) <2014>  <Wei Liu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published bythe Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 *
 * RML_Speed_Smoother.cpp
 *
 *  Created on: 18 Sep, 2014
 *      Author: liuwlz
 */


#include <Speed_Smoother/RML_Speed_Smoother.hpp>

