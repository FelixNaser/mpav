/* Copyright (C) <2014>  <Wei Liu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 *
 * RML_Speed_Smoother.hpp
 *
 *  Created on: 18 Sep, 2014
 *      Author: liuwlz
 */

#ifndef RML_SPEED_SMOOTHER_HPP_
#define RML_SPEED_SMOOTHER_HPP_

#include <ros/ros.h>

namespace Control {

	class RMLSpeedSmoother {
	public:
		RMLSpeedSmoother();
		virtual ~RMLSpeedSmoother();

	private:

		ros::NodeHandle nh_, pri_nh_;

	};

}  // namespace Control


#endif /* RML_SPEED_SMOOTHER_HPP_ */
