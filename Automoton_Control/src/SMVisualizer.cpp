/* Copyright (C) <2014>  <Wei Liu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published bythe Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 *
 * SMVisualizer.cpp
 *
 *  Created on: 25 Sep, 2014
 *      Author: liuwlz
 */

#include <Automoton_Control/SMVisualizer.hpp>

namespace BehaviorPlan {
	SMVisualizer::
	SMVisualizer():
	pri_nh_("~"){

		pri_nh_.param("global_frame",global_frame_,string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));

		status_markerarray_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("navigation_status",1);
		visualize_timer_ = nh_.createTimer(ros::Duration(0.05),&SMVisualizer::visualizeSMTimer,this);

		sm_status_marker_ = shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
		speed_status_marker_ = shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
		vehicle_marker_ = shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
		status_markerarray_= shared_ptr<visualization_msgs::MarkerArray>(new visualization_msgs::MarkerArray());

		makeMarker(sm_status_marker_,1);
		makeMarker(speed_status_marker_,2);
		makeMarker(vehicle_marker_,3);
	}

	SMVisualizer::
	~SMVisualizer(){

	}

	void
	SMVisualizer::
	visualizeSMTimer(const ros::TimerEvent& e){
		status_markerarray_->markers.clear();
		status_markerarray_->markers.push_back(*sm_status_marker_);
		//status_markerarray_->markers.push_back(*speed_status_marker_);
		status_markerarray_->markers.push_back(*vehicle_marker_);
		status_markerarray_pub_.publish(*status_markerarray_);
	}

	void
	SMVisualizer::
	makeMarker(visualization_msgs::MarkerPtr& markerIn, int markerType){
		markerIn->header.frame_id = base_frame_;
		markerIn->header.stamp = ros::Time::now();
		markerIn->action = visualization_msgs::Marker::ADD;
		markerIn->lifetime = ros::Duration(0.0);

		if(markerType == 1){
			markerIn->ns = "SM_Status";
			markerIn->type = visualization_msgs::Marker::TEXT_VIEW_FACING;
			markerIn->text = "Unknown";
			markerIn->scale.z = 2.0;
			markerIn->color.r = 0.0;
			markerIn->color.g = 0.0;
			markerIn->color.b = 1.0;
			markerIn->color.a = 1.0;
			markerIn->pose.position.x = -2.5;
			markerIn->pose.position.y = 0.0;
			markerIn->pose.orientation.w = 1.0;
		}

		if(markerType == 2){
			markerIn->ns = "Speed_Status";
			markerIn->type = visualization_msgs::Marker::TEXT_VIEW_FACING;
			markerIn->text = "Unknown";
			markerIn->scale.z = 2.0;
			markerIn->color.r = 0.0;
			markerIn->color.g = 0.0;
			markerIn->color.b = 1.0;
			markerIn->color.a = 1.0;
			markerIn->pose.position.x = 0.0;
			markerIn->pose.position.y = -2.0;
			markerIn->pose.orientation.w = 1.0;
		}

		if(markerType == 3){
			markerIn->ns = "Vehicle_Model";
			markerIn->type = visualization_msgs::Marker::MESH_RESOURCE;
			markerIn->mesh_resource = "package://Automoton_Control/golfcart.STL";
			markerIn->scale.x = 0.025;
			markerIn->scale.y = 0.025;
			markerIn->scale.z = 0.025;
			markerIn->color.r = 0.8;
			markerIn->color.g = 0.69;
			markerIn->color.b = 0.584;
			markerIn->color.a = 1.0;

			markerIn->pose.position.x = 1.8;
			markerIn->pose.position.y = 0.6;

			markerIn->pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0,0.0,3.1415926) ;
		}
	}
}  // namespace BehaviorPlan


