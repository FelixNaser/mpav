/*
 * MissionWaitingSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef MISSIONWAITINGSM_HPP_
#define MISSIONWAITINGSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan {

	struct MissionWaitingSM:sc::state<MissionWaitingSM, NaviSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_MissionRecived>
				>reactions;

		typedef sc::state<MissionWaitingSM, NaviSM> base;
		MissionWaitingSM(my_context ctx):base(ctx){
			ROS_WARN("Enter MissionWaiting");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "MissionWaiting";
			context<NaviSM>().sm_msg_.CurrentStatus = Mission;

			if (context<NaviSM>().metric_map_type_==1)
				context<NaviSM>().prior_metric_map_->metric_map_timer_.stop();
			else
				context<NaviSM>().extend_metric_map_->metric_map_timer_.stop();

			context<NaviSM>().simulate_speed_ctr_->speed_control_timer_.stop();
			context<NaviSM>().pp_steering_ctr_->steer_control_timer_.stop();
			context<NaviSM>().replan_pp_steering_ctr_->steer_control_timer_.stop();
			context<NaviSM>().replan_pp_steering_ctr_->setLookAheadDist(1.2);
			context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.stop();
			context<NaviSM>().obst_replan_trigger_->trigger_reason_timer_.stop();
			context<NaviSM>().reedshepp_planner_->plan_timer_.stop();
			context<NaviSM>().dubins_planner_->plan_timer_.stop();
		}

		virtual ~MissionWaitingSM(){

		}

		sc::result react(const Ev_MissionRecived& ){
			return transit<InitPerceptionSM>();
		}
	};

}  // namespace MissionWaitingSM

#endif /* MISSIONWAITINGSM_HPP_ */
