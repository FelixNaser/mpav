/*
 * NavigationSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef NAVIGATIONSM_HPP_
#define NAVIGATIONSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>
#include <nav_msgs/Path.h>

using namespace MissionPlan;
using namespace MotionPlan;
using namespace Perception;
using namespace Control;
using namespace Util;

namespace BehaviorPlan{

	struct NaviSM:sc::state_machine<NaviSM, IdleCheckSM>{

		NaviSM(){
			ROS_WARN("Welcome Aboard!");
		};
		virtual ~NaviSM(){};

		nav_msgs::Path reference_path_, modifed_path_;
		Automoton_Control::SM_Msg sm_msg_;

		shared_ptr<SMVisualizer> sm_visualizer_;
		shared_ptr<StationIntegration> station_integration_;
		shared_ptr<ExtendMetricMap> extend_metric_map_;
		shared_ptr<PriorMetricMap> prior_metric_map_;
		shared_ptr<PPSteeringCtr> pp_steering_ctr_;
		shared_ptr<PPSteeringCtr> replan_pp_steering_ctr_;
		shared_ptr<StanleySteeringCtr> stanley_steer_ctr_;
		shared_ptr<SpeedControl<SimulateSpeedAdvisor> > simulate_speed_ctr_;
		shared_ptr<ObstReactReplanTrigger> obst_replan_trigger_;
		shared_ptr<RRTSPlanner> dubins_planner_;
		shared_ptr<RRTSPlanner> reedshepp_planner_;
		shared_ptr<ObstAvoidGoal> obst_avoid_goal_;
		shared_ptr<UTurnGoal> uturn_goal_;

		int metric_map_type_, planner_type_, steering_type_, uturn_type_;
	};
}

#endif /* NAVIGATIONSM_HPP_ */
