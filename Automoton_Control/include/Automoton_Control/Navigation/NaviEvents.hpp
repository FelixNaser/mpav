/*
 * NaviEvents.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NAVIEVENTS_HPP_
#define NAVIEVENTS_HPP_

#include <boost/statechart/event.hpp>

namespace sc = boost::statechart;

namespace BehaviorPlan{

	/**
	 *	Navi level events
	 */
	struct Ev_VehicleReady:sc::event<Ev_VehicleReady>{

	};

	struct Ev_MissionRecived:sc::event<Ev_MissionRecived>{

	};

	struct Ev_PerceptionInitilized:sc::event<Ev_PerceptionInitilized>{

	};

	struct Ev_ObstAvoid:sc::event<Ev_ObstAvoid>{

	};

	struct Ev_Parking:sc::event<Ev_Parking>{

	};

	struct Ev_ReachDest:sc::event<Ev_ReachDest>{

	};

	struct Ev_UTurn:sc::event<Ev_UTurn>{

	};

	/**
	 * ObstAvoid level events
	 */
	struct Ev_ReplanPathFound:sc::event<Ev_ReplanPathFound>{

	};

	struct Ev_ReplanPathDanger:sc::event<Ev_ReplanPathDanger>{

	};

	struct Ev_ReachSubGoal:sc::event<Ev_ReachSubGoal>{

	};

	/**
	 * Parking level events
	 */
	struct Ev_ParkingEnd:sc::event<Ev_ParkingEnd>{

	};

	/**
	 * UTurn level events
	 */
	struct Ev_UTurnEnd:sc::event<Ev_UTurnEnd>{

	};

	struct Ev_UTurnPathFound:sc::event<Ev_UTurnPathFound>{

	};

	struct Ev_UTurnPathDanger:sc::event<Ev_UTurnPathDanger>{

	};
}



#endif /* NAVIEVENTS_HPP_ */
