/*
 * IdleCheckSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef IDLECHECKSM_HPP_
#define IDLECHECKSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan{

	struct IdleCheckSM:sc::state<IdleCheckSM, NaviSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_VehicleReady>
		>reactions;

		typedef sc::state<IdleCheckSM, NaviSM> base;
		IdleCheckSM(my_context ctx):base(ctx){
			ROS_WARN("Enter IdleCheck");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "IdleCheck";
			post_event(Ev_VehicleReady());
		}

		virtual ~IdleCheckSM(){

		}

		sc::result react(const Ev_VehicleReady& ){
			return transit<MissionWaitingSM>();
		}
};

}

#endif /* IDLECHECKSM_HPP_ */
