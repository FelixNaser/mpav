/*
 * NavigationSMNode.hpp
 *
 *  Created on: 29 Nov, 2013
 *      Author: liuwlz
 */

#ifndef NAVIGATIONSMNODE_HPP_
#define NAVIGATIONSMNODE_HPP_

#include <ros/ros.h>
#include <boost/shared_ptr.hpp>

#include <Automoton_Control/Navigation/NavigationSM.hpp>
#include <Automoton_Control/IdelCheck/IdleCheckSM.hpp>
#include <Automoton_Control/MissionWaiting/MissionWaitingSM.hpp>
#include <Automoton_Control/InitPerception/InitPerception.hpp>
#include <Automoton_Control/Moving/MovingSM.hpp>
#include <Automoton_Control/NormPlan/NormPlan.hpp>
#include <Automoton_Control/ObstAvoid/ObstAvoidSM.hpp>
#include <Automoton_Control/UTurn/UTurnSM.hpp>
#include <Automoton_Control/Parking/ParkingSM.hpp>

#include <Automoton_Control/UTurnService.h>

using namespace boost;

namespace BehaviorPlan{
	class NaviStateMachine{
	public:
		NaviStateMachine();
		~NaviStateMachine();

	private:
		ros::NodeHandle nh_, pri_nh_;
		ros::Timer status_check_timer_;
		ros::Subscriber reference_path_sub_;
		ros::ServiceServer uturn_service_;
		ros::Time replan_time_;

		bool obst_replan_;
		bool init_replan_timer_;
		double goal_reach_dist_, replan_duration_tolerance_;

		shared_ptr<NaviSM> navi_sm_;

		void referencePathCallBack(const nav_msgs::Path& pathIn);
		void statusCheckTimer(const ros::TimerEvent& e);
		bool uTurnServiceCallBack(Automoton_Control::UTurnService::Request& reqIn,
				Automoton_Control::UTurnService::Response& resOut);

		void checkReplanTimeOut();
	};
}

#endif /* NAVIGATIONSMNODE_HPP_ */
