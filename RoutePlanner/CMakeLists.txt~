cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)

rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

INCLUDE(FindPkgConfig)
pkg_check_modules(TINYXML REQUIRED tinyxml)
pkg_check_modules(TINYXML26 tinyxml>=2.6) #QUIET only works with cmake>=2.8.2
if( NOT TINYXML26_FOUND )
    SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DTINYXML_API_PRE26")
    MESSAGE("Using old tinyxml API (pre 2.6). Setting the compilation flag TINYXML_API_PRE26.")
endif( NOT TINYXML26_FOUND )
INCLUDE_DIRECTORIES(${TINYXML_INCLUDE_DIRS})
LINK_DIRECTORIES(${TINYXML_LIBRARY_DIRS})

FIND_PACKAGE(Boost REQUIRED COMPONENTS filesystem)
IF (BOOST_FOUND)
    INCLUDE_DIRECTORIES(${BOOST_INCLUDE_DIR})
ENDIF()

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

#common commands for building c++ executables and libraries
rosbuild_add_library(${PROJECT_NAME} 
src/SvgPath.cpp 
src/StationLoading.cpp
src/StationSearching.cpp
src/StationRouting.cpp
src/StationInteractive.cpp
src/StationIntegration.cpp
)
TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${TINYXML_LIBRARIES} ${CURL_LIBRARIES} ${Boost_FILESYSTEM_LIBRARY})

#target_link_libraries(${PROJECT_NAME} another_library)
rosbuild_add_boost_directories()
rosbuild_link_boost(${PROJECT_NAME} thread)
rosbuild_add_executable(Route_Planner src/main.cpp)
target_link_libraries(Route_Planner ${PROJECT_NAME})
