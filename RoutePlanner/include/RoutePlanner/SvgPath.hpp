/*
 * SvgPath.hpp
 *
 *  Created on: 11 Jul, 2014
 *      Author: liuwlz
 */

#ifndef SVGPATH_HPP_
#define SVGPATH_HPP_

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <math.h>
#include <float.h>

#include <tinyxml.h>
#include <wordexp.h>
#include <vector>
#include <boost/shared_ptr.hpp>

#define RoutePlanDebug false

using namespace std;
using namespace boost;

#ifdef TINYXML_API_PRE26
#define TINYXML_ELEMENT ELEMENT
#define TINYXML_TEXT TEXT
#endif

namespace MissionPlan{

	struct PathPoint{
		PathPoint(){};
		PathPoint(double x, double y):x_(x),y_(y){};
		double x_, y_;
	};

	class StationPath{
	public:
		StationPath();
		~StationPath(){point_set_.clear();};

		StationPath& operator=(const StationPath &stationPathIn);
		StationPath& operator+(const StationPath &stationPathIn);

		bool exist_;
		vector<PathPoint> point_set_;
		double distance_;
		double speed_limit_;
		double congestion_;
	};

	typedef pair<string, PathPoint> StationPair;

	class SvgPath{
	public:
		SvgPath();
		SvgPath(const char* fileNameIn,double resolution);
		virtual ~SvgPath();

		virtual void loadFile(const char* fileNameIn);
		virtual void getResolution();
		virtual void getStationList(vector<StationPair> &stationListOut);
		virtual StationPath getStationPath(string pathNameIn);
		virtual bool checkStationPath(string pathNameIn);
		virtual double getPathLength(vector<PathPoint>& pathPointIn);
		virtual double getSpeedLimit(string pathNameIn){return 0.0;};

	private:

		double resolution_;

		shared_ptr<TiXmlDocument> svg_doc_;
		string tildeExpand(const char *path);
		vector<PathPoint> stringToPath(string stringIn);
		vector<string> SplitString(const char *stringIn, const char* delimiter);
		PathPoint getSize();
	};
}



#endif /* SVGPATH_HPP_ */
