/*
 * StationRouting.hpp
 *
 *  Created on: 3 Jul, 2014
 *      Author: liuwlz
 */

#ifndef STATIONROUTING_HPP_
#define STATIONROUTING_HPP_

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>

#include <iostream>

#include <RoutePlanner/StationSeaching.hpp>

namespace MissionPlan {
	class StationRouting {
	public:
		StationRouting();
		virtual ~StationRouting();

		void initStation();
		virtual void prompt(bool stateMachine);
		virtual void prompt(int startIn, int endIn);
		void routing(int startIn, int endIn);
		void visStation();

		nav_msgs::Path path_;
		vector<int> station_index;
		vector<string> station_name;
		shared_ptr<StationSearching> station_searching_;

		string global_frame_, base_frame_;
		StationPath station_path_;

		ros::NodeHandle nh_, priv_nh_;
		ros::Publisher path_pub_, station_marker_pub_;
	};
}  // namespace MissionPlan



#endif /* STATIONROUTING_HPP_ */
