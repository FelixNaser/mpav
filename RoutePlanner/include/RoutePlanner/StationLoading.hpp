/*
 * StationLoading.hpp
 *
 *  Created on: 2 Jul, 2014
 *      Author: liuwlz
 */

#ifndef STATIONLOADING_HPP_
#define STATIONLOADING_HPP_

#include <string.h>
#include <stdlib.h>
#include <utility>
#include <algorithm>

#include <boost/graph/graph_utility.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/shared_ptr.hpp>

#include <RoutePlanner/SvgPath.hpp>

using namespace std;
using namespace boost;

#define SVG_ADD "~/SvgPath/path_def.svg"
#define RESOLUTION 0.1

namespace MissionPlan {

	struct Station{
		int id;
		string name;
		PathPoint address;
		int popularity;
	};

	struct Route{
		string name;
		double distance;
		double speed_limit;
		double congestion;
	};

	typedef adjacency_matrix<directedS, Station, Route> RouteMap;
	typedef property_map<RouteMap, vertex_index_t>::type IndexMap;
	typedef RouteMap::vertex_iterator VertexIter;
	typedef RouteMap::out_edge_iterator OutEdgeIter;
	typedef RouteMap::edge_iterator EdgeIter;
	typedef RouteMap::vertex_descriptor VertexDpter;
	typedef RouteMap::edge_descriptor EdgeDpter;
	typedef std::pair<int, int> Edge;

	class StationLoading {

	private:
		int num_route_, num_station_;
		vector<StationPair> station_list;
		vector<Edge> route_list;

		shared_ptr<SvgPath> svg_path_;
		shared_ptr<RouteMap> route_map_;

	public:
		StationLoading();
		~StationLoading();

		void loadStationList();
		void loadRouteList();
		void loadProperty(shared_ptr<RouteMap>& routeMapIn);

		string getRouteName(int startID, int endIn);

		vector<StationPair> getStationList() const {return station_list;};

		string getStationName(int stationID);
		vector<string> getStationName();

		PathPoint getStationAddress(int stationID);
		vector<PathPoint> getStationAddress();

		vector<int> getStationNeighbour(int stationID);

		StationPath getStationPath(int startID, int endIn);
	};
}  // namespace MissionPlan

#endif /* STATIONLOADING_HPP_ */
