/*
 * StationIntegration.hpp
 *
 *  Created on: 24 Jul, 2014
 *      Author: liuwlz
 */

#ifndef STATIONINTEGRATION_HPP_
#define STATIONINTEGRATION_HPP_

#include <ros/ros.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <visualization_msgs/InteractiveMarkerControl.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>
#include <interactive_markers/interactive_marker_server.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/tfMessage.h>

#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <boost/bind.hpp>
#include <RoutePlanner/StationRouting.hpp>

using namespace boost;
using namespace visualization_msgs;
using namespace interactive_markers;

namespace MissionPlan {

	typedef pair<int, int> StationCouple;

	class StationIntegration:public StationRouting{

	public:
		StationIntegration();
		virtual ~StationIntegration();

		void initFirstStation();
		void setMissionActive(bool activeIn){mission_active_ = activeIn;};

	private:

		vector<int> parking_stations_;
		vector<StationCouple> station_paired_;
		StationCouple mission_station_, station_select_;
		bool first_station_initialized_, mission_active_;

		tf::TransformListener tf_;

		geometry_msgs::PoseStamped vehicle_pose_;
		shared_ptr<InteractiveMarkerServer> server_;

		void initPairedStation();
		void initInteractiveMarker();
		void routingCouple();
		void routingParking();
		Marker makeMarker(InteractiveMarker& intMarkerIn);
		void processStationFeedback(const InteractiveMarkerFeedbackConstPtr &feedbackIn);
		geometry_msgs::Pose getMarkerPose(int stationID);
		bool getVehiclePose();

		double getVehiclePathPointDist(const PathPoint& pointInA,  const PathPoint& pointInB);
		void vehicelPoseCallBack(const geometry_msgs::PoseWithCovarianceStampedConstPtr poseIn);
	};

}  // namespace MissionPlan

#endif /* STATIONINTEGRATION_HPP_ */
