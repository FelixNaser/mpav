/*
 * StationIntegration.cpp
 *
 *  Created on: 24 Jul, 2014
 *      Author: liuwlz
 */

#include <RoutePlanner/StationIntegration.hpp>

namespace MissionPlan{
	StationIntegration::
	StationIntegration():
	StationRouting(),
	first_station_initialized_(false),
	mission_active_(true){

		server_ = shared_ptr<InteractiveMarkerServer>(new InteractiveMarkerServer("RoutePlannerMarker"));

		mission_station_.first = -1; mission_station_.second = -1;
		while(!getVehiclePose()){
			ROS_INFO("Failed to get vehicle pose, keep trying!");
		}
		initInteractiveMarker();
	}

	StationIntegration::
	~StationIntegration(){

	}

	bool
	StationIntegration::
	getVehiclePose(){
		tf::Stamped<tf::Pose> global_pose;
		global_pose.setIdentity();
		tf::Stamped<tf::Pose> base_pose;
		base_pose.setIdentity();
		base_pose.frame_id_ = base_frame_;
		base_pose.stamp_ = ros::Time();
		ros::Time current_time = ros::Time::now(); // save time for checking tf delay later

		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time::now(),ros::Duration(0.001));
			tf_.transformPose(global_frame_, base_pose, global_pose);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		tf::poseStampedTFToMsg(global_pose, vehicle_pose_);
		initPairedStation();
		initFirstStation();
		return true;
	}

	void
	StationIntegration::
	initPairedStation(){
		for (vector<int>::iterator iter = station_index.begin();
				iter!= station_index.end();
				iter++){
			//Find the parking lot station
			if (strncmp(station_name[*iter].c_str(),"ParkLot",7) == 0){
				parking_stations_.push_back(*iter);
				continue;
			}

			//Find the coupled stations that coded as "*" and "Opp*"
			if (strncmp(station_name[*iter].c_str(),"Opp",3)==0)
				continue;
			else{
				bool pair_found = false;;
				StationCouple current_pair;
				current_pair.first = *iter;
				for (vector<int>::iterator iter_opp = station_index.begin();
						iter_opp!= station_index.end();
						iter_opp++){
					if (strncmp(station_name[*iter_opp].c_str(),"Opp",3)==0){
						if (station_name[*iter_opp].compare(3,station_name[*iter].length(),station_name[*iter])==0){
							current_pair.second = *iter_opp;
							pair_found = true;
						}
					}
					else
						continue;
				}
				if (!pair_found){
					current_pair.second = *iter;
				}
				station_paired_.push_back(current_pair);
			}
		}
	}

#define SQ(x) ((x)*(x))
	double
	StationIntegration::
	getVehiclePathPointDist(const PathPoint& pointInA, const PathPoint& pointInB){
		double path_pts_theta = atan2(pointInB.y_-pointInA.y_, pointInB.x_ - pointInA.x_);
		geometry_msgs::Pose& veh_pose = vehicle_pose_.pose;
		double roll=0, pitch=0, yaw=0;
		tf::Quaternion q;
		tf::quaternionMsgToTF(veh_pose.orientation, q);
		tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
		double angle_dist = fabs(yaw - path_pts_theta);
		double linear_dist = sqrt(SQ(veh_pose.position.x - pointInA.x_)+SQ(veh_pose.position.y - pointInA.y_));
		return (linear_dist + 2*angle_dist);
	}

	void
	StationIntegration::
	initFirstStation(){
		double min_dist = DBL_MAX;
		cout << "Station size"<< station_name.size()<<endl;
		for (vector<string>::iterator iter = station_name.begin();
				iter != station_name.end();
				iter++){
			vector<int> neighbour = station_searching_->getStationLoading()->getStationNeighbour(iter - station_name.begin());
			for (vector<int>::iterator iter_inner = neighbour.begin();
					iter_inner!= neighbour.end();
					iter_inner++){

				StationPath station_path_tmp = station_searching_->getRoute(iter - station_name.begin(),*iter_inner);

				for (vector<PathPoint>::iterator iter_pts = station_path_tmp.point_set_.begin();
						iter_pts != station_path_tmp.point_set_.end()-1;
						iter_pts ++){
					double curr_dist = getVehiclePathPointDist(*iter_pts, *(iter_pts+1));
					if (curr_dist < min_dist){
						min_dist = curr_dist;
						mission_station_.first = iter-station_name.begin();
					}
				}
			}
		}
		first_station_initialized_ = true;
		cout <<"Will start from station: "<< station_name[mission_station_.first]<< " and ready for transform"<<endl;
	}

	void
	StationIntegration::
	routingCouple(){
		cout << "Selected Station: "<<station_name[station_select_.first]<< " or "<< station_name[station_select_.second] <<endl;

		StationPath station_path_first = station_searching_->getRoute(mission_station_.first, station_select_.first);
		StationPath station_path_second = station_searching_->getRoute(mission_station_.first, station_select_.second);

		station_path_ = station_path_first.distance_ < station_path_second.distance_ ? station_path_first :station_path_second;
		mission_station_.first = station_path_first.distance_ < station_path_second.distance_ ? station_select_.first: station_select_.second;

		path_.poses.clear();
		for (vector<PathPoint>::iterator iter = station_path_.point_set_.begin();
				iter != station_path_.point_set_.end();
				iter++){
			geometry_msgs::PoseStamped current_pose;
			current_pose.pose.position.x = iter->x_;
			current_pose.pose.position.y = iter->y_;
			current_pose.pose.orientation.w = 1.0;
			path_.poses.push_back(current_pose);
		}
		path_.header.frame_id = global_frame_;
		path_.header.stamp = ros::Time::now();
		path_pub_.publish(path_);
	}

	void
	StationIntegration::
	routingParking(){
		if (!first_station_initialized_)
			return;
		ROS_INFO("Routing the route to parking lot");
		StationPath parking_path;
		int parking_id = -1;
		if ((int)parking_stations_.size() == 0){
			ROS_WARN("No available Parking lots in this area");
			return;
		}
		for (vector<int>::iterator iter_parking = parking_stations_.begin();
				iter_parking != parking_stations_.end();
				iter_parking++){
			StationPath tmp_path = station_searching_->getRoute(mission_station_.first, *iter_parking);
			if (tmp_path.distance_ < parking_path.distance_){
				parking_path = tmp_path;
				parking_id = *iter_parking;
			}
		}

		station_path_ = parking_path;
		mission_station_.first = parking_id;

		path_.poses.clear();
		for (vector<PathPoint>::iterator iter = station_path_.point_set_.begin();
				iter != station_path_.point_set_.end();
				iter++){
			geometry_msgs::PoseStamped current_pose;
			current_pose.pose.position.x = iter->x_;
			current_pose.pose.position.y = iter->y_;
			current_pose.pose.orientation.w = 1.0;
			path_.poses.push_back(current_pose);
		}
		path_.header.frame_id = global_frame_;
		path_.header.stamp = ros::Time::now();
		path_pub_.publish(path_);
	}

	Marker
	StationIntegration::
	makeMarker(InteractiveMarker& intMarkerIn){
		MarkerPtr station_marker = MarkerPtr(new Marker());
		station_marker->type = visualization_msgs::Marker::TEXT_VIEW_FACING;
		station_marker->scale.x = 15*intMarkerIn.scale;
		station_marker->scale.y = 15*intMarkerIn.scale;
		station_marker->scale.z = 15;
		station_marker->text = intMarkerIn.name;

		station_marker->color.r = 1.0;
		station_marker->color.g = 0.0;
		station_marker->color.b = 0.0;
		station_marker->color.a = 1.0;
		return *station_marker;
	}

	void
	StationIntegration::
	processStationFeedback(const InteractiveMarkerFeedbackConstPtr &feedbackIn){
		if (feedbackIn->event_type == InteractiveMarkerFeedback::BUTTON_CLICK){
			station_select_ = station_paired_[atoi(feedbackIn->control_name.c_str())];
			if (mission_active_){
				routingCouple();
			}
		}
	}

	void
	StationIntegration::
	initInteractiveMarker(){
		for(vector<StationCouple>::iterator iter = station_paired_.begin();
			iter != station_paired_.end();
			iter++){
			InteractiveMarkerPtr int_marker = InteractiveMarkerPtr(new InteractiveMarker());
			int_marker->header.frame_id = global_frame_;
			int_marker->header.stamp = ros::Time::now();
			int_marker->pose = getMarkerPose(iter->first);
			int_marker->scale = 1;
			int_marker->name = station_name[iter->first];
			int_marker->description = "Button\n(Left Click)";

			InteractiveMarkerControlPtr control = InteractiveMarkerControlPtr(new InteractiveMarkerControl());
			control->interaction_mode = InteractiveMarkerControl::BUTTON;
			control->name = boost::lexical_cast<string>(iter-station_paired_.begin());
			MarkerPtr marker = MarkerPtr(new Marker(makeMarker(*int_marker)));
			control->markers.push_back(*marker);
			control->always_visible = true;

			int_marker->controls.push_back(*control);

			server_->insert(*int_marker);
			server_->setCallback(int_marker->name, bind(&StationIntegration::processStationFeedback, this, _1));
			server_->applyChanges();
		}
	}

	geometry_msgs::Pose
	StationIntegration::
	getMarkerPose(int stationID){
		geometry_msgs::Pose pose_out;
		pose_out.position.x = station_searching_->getStationLoading()->getStationAddress(stationID).x_;
		pose_out.position.y = station_searching_->getStationLoading()->getStationAddress(stationID).y_;
		pose_out.orientation.w = 1.0;

		return pose_out;
	}

}  // namespace MissionPlan
