/*
 * StationLoading.cpp
 *
 *  Created on: 2 Jul, 2014
 *      Author: liuwlz
 */




#include <RoutePlanner/StationLoading.hpp>


namespace MissionPlan{

	StationLoading::
	StationLoading():
	num_route_(0),
	num_station_(0){
		loadStationList();
		loadRouteList();
	}

	StationLoading::
	~StationLoading(){

	}

	void
	StationLoading::
	loadStationList(){
		svg_path_ = shared_ptr<SvgPath>(new SvgPath(string(SVG_ADD).c_str(),RESOLUTION));
	    svg_path_->getStationList(station_list);
	    num_station_ = station_list.size();
	}

	void
	StationLoading::
	loadRouteList(){
		for (vector<StationPair>::iterator iter = station_list.begin(); iter!= station_list.end(); iter++){
			int curr_index = iter - station_list.begin();
			if(RoutePlanDebug) cout <<"Current Station Name: "<< iter->first <<endl;
			for (int other_index = 0 ; other_index < num_station_; other_index++){
				string route_name = getRouteName(curr_index, other_index);
			    if (svg_path_->checkStationPath(route_name)){
			    	route_list.push_back(Edge(curr_index, other_index));
			    	num_route_++;
			    }
			}
		}
		if(RoutePlanDebug) cout <<"Enquiring Route Done, find "<<num_route_<<" routes"<<endl;
	}

	void
	StationLoading::
	loadProperty(shared_ptr<RouteMap>& routeMapIn){
		if(RoutePlanDebug) cout <<"loadProperty"<<endl;

		if (routeMapIn == NULL){
			routeMapIn = shared_ptr<RouteMap>(new RouteMap(num_station_));
			for (int i = 0 ; i < num_route_; i++){
				add_edge(route_list[i].first, route_list[i].second, *routeMapIn);
			}
		}

		VertexDpter vd;
		EdgeDpter ed;
		OutEdgeIter out_i, out_end;
		EdgeIter e_i, e_end;

		for (int i = 0; i < num_station_; i++){
			vd = *vertices(*routeMapIn).first + i;
			(*routeMapIn)[vd].id = i;
			(*routeMapIn)[vd].name = station_list[i].first;
			(*routeMapIn)[vd].popularity = 1.0;
			(*routeMapIn)[vd].address = station_list[i].second;

			if (RoutePlanDebug)
				cout <<"OutEdge size: "<< out_degree(vd,*routeMapIn)<<endl;
			for (tie(out_i, out_end) = out_edges(vd, *routeMapIn); out_i != out_end; out_i++){
				ed = *out_i;
				(*routeMapIn)[ed].name = getRouteName(i, target(*out_i, *routeMapIn));
				shared_ptr<StationPath> path_tmp = shared_ptr<StationPath>(new StationPath());
				*path_tmp = svg_path_->getStationPath((*routeMapIn)[ed].name);
				(*routeMapIn)[ed].distance = path_tmp->distance_;
				(*routeMapIn)[ed].speed_limit = path_tmp->speed_limit_;
			}
		}

		if (RoutePlanDebug){
			for (int i = 0; i < num_station_ && RoutePlanDebug; i++){
				vd = *vertices(*routeMapIn).first + i;
				cout << (*routeMapIn)[vd].name <<endl;
			}

			for (tie(e_i, e_end)=edges(*routeMapIn); e_i != e_end; e_i++){
				ed = *e_i;
				cout << (*routeMapIn)[ed].name <<"//";
				cout << (*routeMapIn)[ed].distance <<endl;
			}
		}
	}

	string
	StationLoading::
	getRouteName(int startIn, int endIn){
		char route_name[strlen(station_list[startIn].first.c_str()) + strlen(station_list[endIn].first.c_str())+1];
	    strcpy(route_name, station_list[startIn].first.c_str());
	    strcat(route_name, "_");
	    strcat(route_name, station_list[endIn].first.c_str());
	    return string(route_name);
	}

	string
	StationLoading::
	getStationName(int stationID){
		return (station_list[stationID].first);
	}

	vector<string>
	StationLoading::
	getStationName(){
		vector<string> station_name;
		for (int i = 0 ; i < num_station_; i++)
			station_name.push_back(station_list[i].first);
		return station_name;
	}

	PathPoint
	StationLoading::
	getStationAddress(int stationID){
		return (station_list[stationID].second);
	}

	vector<PathPoint>
	StationLoading::
	getStationAddress(){
		vector<PathPoint> station_address;
		for (int i = 0 ; i < num_station_; i++)
			station_address.push_back(station_list[i].second);
		return station_address;
	}

	StationPath
	StationLoading::
	getStationPath(int startID, int endIn){
		string route_name = getRouteName(startID, endIn);
		shared_ptr<StationPath> path_tmp = shared_ptr<StationPath>(new StationPath());
		*path_tmp = svg_path_->getStationPath(route_name);
		return *path_tmp;
	}

	vector<int>
	StationLoading::
	getStationNeighbour(int stationID){
		vector<int> neighbour_out;
		for (vector<Edge>::iterator iter = route_list.begin();
				iter != route_list.end();
				iter ++){
			if (iter->first == stationID)
				neighbour_out.push_back(iter->second);
		}
		return neighbour_out;
	}
}  // namespace MissionPlan
