/*
 * StationRouting.cpp
 *
 *  Created on: 3 Jul, 2014
 *      Author: liuwlz
 */




#include <RoutePlanner/StationRouting.hpp>

namespace MissionPlan{
	StationRouting::
	StationRouting():
	priv_nh_("~"){

		station_searching_ = shared_ptr<StationSearching>(new StationSearching());
		priv_nh_.param("base_frame", base_frame_, string("base_link"));
		priv_nh_.param("global_frame", global_frame_, string("map"));
		path_pub_ = nh_.advertise<nav_msgs::Path>("route_plan", 1, this);
		station_marker_pub_ = nh_.advertise<visualization_msgs::Marker>("station_marker",1, this);
		this->initStation();
	}

	StationRouting::
	~StationRouting(){

	}

	void
	StationRouting::
	routing(int startID, int endID){
		station_path_ = station_searching_->getRoute(startID, endID);
		if (station_path_.exist_ == false)
			return;
		path_.poses.clear();
		for (vector<PathPoint>::iterator iter = station_path_.point_set_.begin();
				iter != station_path_.point_set_.end();
				iter++){
			geometry_msgs::PoseStamped current_pose;
			current_pose.pose.position.x = iter->x_;
			current_pose.pose.position.y = iter->y_;
			current_pose.pose.orientation.w = 1.0;
			path_.poses.push_back(current_pose);
		}
		path_.header.frame_id = global_frame_;
		path_.header.stamp = ros::Time::now();
		path_pub_.publish(path_);
	}

	void
	StationRouting::
	initStation(){
		station_name = station_searching_->getStationLoading()->getStationName();
		for (vector<string>::iterator iter = station_name.begin(); iter != station_name.end(); iter++){
			if (strncmp(iter->c_str(), "Link", 4) != 0){
				station_index.push_back(iter - station_name.begin());
			}
		}
		visStation();
	}

	void
	StationRouting::
	prompt(int startIn, int endIn){
		for (vector<int>::iterator iter = station_index.begin(); iter != station_index.end(); iter++){
			cout << "Station ID: "<< iter - station_index.begin() << " Name: " << station_name[*iter] << endl;
		}
		routing(station_index[startIn], station_index[endIn]);
	}

	void
	StationRouting::
	prompt(bool stateMachine){
		for (vector<int>::iterator iter = station_index.begin(); iter != station_index.end(); iter++){
			cout << "Station ID: "<< iter - station_index.begin() << " Name: " << station_name[*iter] << endl;
		}

		int start, end;
		cout<<"Please type in the start station: "<<endl;
		cin >> start;
		while (start > (int)station_index.size()-1){
			cerr << "Please type in valid start station number"<<endl;
			cin >> start;
		}
		cout<<"Please type in the end station: "<<endl;
		cin >> end;
		while (end > (int)station_index.size()-1){
			cerr << "Please type in valid end station number"<<endl;
			cin >> end;
		}
		routing(station_index[start], station_index[end]);
		if (!stateMachine){
			cout <<"Press Enter when you want new route! Press any other key to exit!"<<endl;
			cin.ignore(1);
			if (cin.get() == '\n')
				prompt(false);
			else
				exit(0);
		}
	}

	void
	StationRouting::
	visStation(){
		visualization_msgs::MarkerPtr station_marker = visualization_msgs::MarkerPtr(new visualization_msgs::Marker());
		station_marker->header.frame_id = global_frame_;
		station_marker->header.stamp = ros::Time::now();
		station_marker->ns = "Station";
		station_marker->action = visualization_msgs::Marker::ADD;
		station_marker->type = visualization_msgs::Marker::SPHERE_LIST;
		station_marker->scale.x = 15;
		station_marker->scale.y = 15;
		station_marker->scale.z = 0.2;

		station_marker->color.r = 1.0;
		station_marker->color.g = 0.0;
		station_marker->color.b = 0.0;
		station_marker->color.a = 1.0;

		for (vector<int>::iterator iter = station_index.begin();
				iter != station_index.end();
				iter ++){
			geometry_msgs::Point point_tmp;
			point_tmp.x = station_searching_->getStationLoading()->getStationAddress(*iter).x_;
			point_tmp.y = station_searching_->getStationLoading()->getStationAddress(*iter).y_;
			point_tmp.z = 0.0;
			station_marker->points.push_back(point_tmp);
		}
		station_marker_pub_.publish(*station_marker);
	}
}
