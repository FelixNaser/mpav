/*
 * StationInteractive.cpp
 *
 *  Created on: 23 Jul, 2014
 *      Author: liuwlz
 */

#include <RoutePlanner/StationInteractive.hpp>

namespace MissionPlan{

	StationInteractive::
	StationInteractive():
	station_select_(-1),
	feedback_counter_(0){
		priv_nh_.param("base_frame", base_frame_, string("base_link"));
		priv_nh_.param("global_frame", global_frame_, string("map"));

		server_ = shared_ptr<InteractiveMarkerServer>(new InteractiveMarkerServer("RoutePlannerMarker"));
		station_routing_ = shared_ptr<StationRouting>(new StationRouting());
		station_pair_.first = -1; station_pair_.second = -1;
		initInteractiveMarker();
	}

	StationInteractive::
	~StationInteractive(){

	}

	void
	StationInteractive::
	prompt(){
		cout << "Selected Station ID: "<< station_select_ << " Name: " << station_routing_->station_name[station_select_] << endl;
		if (station_pair_.first == -1)
			station_pair_.first = station_select_;
		else{
			station_pair_.second = station_select_;
			station_routing_->routing(station_pair_.first, station_pair_.second);
			//station_pair_.first = station_pair_.second;
			station_pair_.first = -1;
			station_pair_.second = -1;
		}
	}

	Marker
	StationInteractive::
	makeMarker(InteractiveMarker& intMarkerIn){
		MarkerPtr station_marker = MarkerPtr(new Marker());
		station_marker->type = visualization_msgs::Marker::SPHERE;
		station_marker->scale.x = 15*intMarkerIn.scale;
		station_marker->scale.y = 15*intMarkerIn.scale;
		station_marker->scale.z = 0.2;

		station_marker->color.r = 1.0;
		station_marker->color.g = 0.0;
		station_marker->color.b = 0.0;
		station_marker->color.a = 1.0;
		return *station_marker;
	}

	void
	StationInteractive::
	processFeedback(const InteractiveMarkerFeedbackConstPtr &feedbackIn){
		if (feedbackIn->event_type == InteractiveMarkerFeedback::BUTTON_CLICK){
			station_select_ = atoi(feedbackIn->control_name.c_str());
			prompt();
		}
	}

	void
	StationInteractive::
	initInteractiveMarker(){
		for(vector<int>::iterator iter = station_routing_->station_index.begin();
			iter != station_routing_->station_index.end();
			iter++){
			InteractiveMarkerPtr int_marker = InteractiveMarkerPtr(new InteractiveMarker());
			int_marker->header.frame_id = global_frame_;
			int_marker->header.stamp = ros::Time::now();
			int_marker->pose = getMarkerPose(*iter);
			int_marker->scale = 1;
			int_marker->name = station_routing_->station_name[*iter];
			int_marker->description = "Button\n(Left Click)";

			InteractiveMarkerControlPtr control = InteractiveMarkerControlPtr(new InteractiveMarkerControl());
			control->interaction_mode = InteractiveMarkerControl::BUTTON;
			control->name = boost::lexical_cast<string>(*iter);
			MarkerPtr marker = MarkerPtr(new Marker(makeMarker(*int_marker)));
			control->markers.push_back(*marker);
			control->always_visible = true;

			int_marker->controls.push_back(*control);

			server_->insert(*int_marker);
			server_->setCallback(int_marker->name, bind(&StationInteractive::processFeedback, this, _1));
			server_->applyChanges();
		}
	}

	geometry_msgs::Pose
	StationInteractive::
	getMarkerPose(int stationID){
		geometry_msgs::Pose pose_out;
		pose_out.position.x = station_routing_->station_searching_->getStationLoading()->getStationAddress(stationID).x_;
		pose_out.position.y = station_routing_->station_searching_->getStationLoading()->getStationAddress(stationID).y_;
		pose_out.orientation.w = 1.0;

		return pose_out;
	}

}  // namespace
