/*
 * Scan_Fusion.cpp
 *
 *  Created on: 28 Jul, 2014
 *      Author: liuwlz
 */


#include <Metric_Map/Scan_Fusion.hpp>

namespace Perception {
	ScanFusion::
	ScanFusion():
	pri_nh_("~"),
	scan_rear_got_(false),
	scan_front_got_(false){
		pri_nh_.param("global_frame",global_frame_, string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));

		laser_sub_front_.subscribe(nh_, "scan_in_front", 10);
		laser_filter_front_ = new tf::MessageFilter<sensor_msgs::LaserScan>(laser_sub_front_, tf_, base_frame_, 10);
		laser_filter_front_->registerCallback(boost::bind(&ScanFusion::laserFrontCallBack, this, _1));

		laser_sub_rear_.subscribe(nh_, "scan_in_rear", 10);
		laser_filter_rear_ = new tf::MessageFilter<sensor_msgs::LaserScan>(laser_sub_rear_, tf_, base_frame_, 10);
		laser_filter_rear_->registerCallback(boost::bind(&ScanFusion::laserRearCallBack, this, _1));

		scan_update_timer_ = nh_.createTimer(ros::Duration(0.05),&ScanFusion::updateScanPts, this);
		scan_pts_pub_ = nh_.advertise<sensor_msgs::PointCloud>("fused_scan_pts",1);

		this->initScanPts();
	}

	ScanFusion::
	~ScanFusion(){

	}

	void
	ScanFusion::
	initScanPts(){
		geometry_msgs::Point32 init_pts;
		init_pts.x = DBL_MAX;
		init_pts.y = DBL_MAX;
		laser_pts_front_.points.push_back(init_pts);
		laser_pts_rear_.points.push_back(init_pts);
	}

	void
	ScanFusion::
	laserFrontCallBack(const sensor_msgs::LaserScanConstPtr scanIn){
		laser_pts_front_.header.stamp = ros::Time::now();
		try{
			projector_.transformLaserScanToPointCloud(base_frame_, *scanIn,
					laser_pts_front_, tf_);
			scan_front_got_ = true;
		}
		catch (tf::TransformException& e){
			ROS_ERROR("ScanFusion %s",e.what());
			return;
		}
	}

	void
	ScanFusion::
	laserRearCallBack(const sensor_msgs::LaserScanConstPtr scanIn){
		laser_pts_rear_.header.stamp = ros::Time::now();
		try{
			scan_rear_got_ = true;
			projector_.transformLaserScanToPointCloud(base_frame_, *scanIn,
					laser_pts_rear_, tf_);
		}
		catch (tf::TransformException& e){
			ROS_ERROR("ScanFusion: %s",e.what());
			return;
		}
	}

	void
	ScanFusion::
	updateScanPts(const ros::TimerEvent& e){
		if (!scan_front_got_ && !scan_rear_got_)
			return;
		laser_pts_fused_.points.clear();
		laser_pts_fused_.points = laser_pts_front_.points;
		laser_pts_fused_.points.insert(laser_pts_fused_.points.end(),laser_pts_rear_.points.begin(),laser_pts_rear_.points.end());
		laser_pts_fused_.header.stamp = ros::Time::now();
		laser_pts_fused_.header.frame_id = base_frame_;
		scan_pts_pub_.publish(laser_pts_fused_);
	}

}  // namespace Perception
