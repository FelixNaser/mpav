/*
 * Prior_Metric_Map.cpp
 *
 *  Created on: 30 Jul, 2014
 *      Author: liuwlz
 */

#include <Metric_Map/Prior_Metric_Map.hpp>

namespace Perception {
	PriorMetricMap::
	PriorMetricMap():
	MetricMap(){
		reference_path_got_ = true;
		initPriorMap();
	}

	PriorMetricMap::
	~PriorMetricMap(){

	}
	
	void
	PriorMetricMap::
	initPriorMap(){
		ROS_INFO("Init Prior Points");
		nav_msgs::GetMap::Request  request;
		nav_msgs::GetMap::Response response;
		ROS_INFO("Requesting the static_map_lane prior map...");
		while(!ros::service::call("/static_map_lane", request, response)){
			ROS_WARN("Prior Metric: Request for map failed; trying again...");
			ros::Duration sleep(0.5);
			sleep.sleep();
			ros::spinOnce();
		}
		*prior_map_ = response.map;
		prior_map_init_ = true;
		ROS_INFO("static_map_lane map retrieved...");
	}

	void
	PriorMetricMap::
	updatePriorInfo(){
		int map_index =0;
		int map_x, map_y;
		prior_pts_->points.clear();
		for (vector<int8_t>::iterator iter = prior_map_->data.begin();
				iter != prior_map_->data.end();
				iter++){

			if (*iter != 100)
				continue;
			map_index = iter - prior_map_->data.begin();

			map_x = map_index%prior_map_->info.width;
			map_y = map_index/prior_map_->info.width;

			geometry_msgs::Point32 map_pose;
			map_pose.x = map_x*prior_map_->info.resolution + prior_map_->info.origin.position.x;
			map_pose.y = map_y*prior_map_->info.resolution + prior_map_->info.origin.position.y;

			prior_pts_->points.push_back(map_pose);
		}
		prior_info_init_ = true;
	}
}  // namespace Perception


