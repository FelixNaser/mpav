/*
 * Dist_Map.cpp
 *
 *  Created on: Dec 19, 2013
 *      Author: liuwlz
 */

#include <Metric_Map/Dist_Map.hpp>

namespace Perception{

	DistMetric::
	DistMetric():
	Max_Dist_(50){

	}

	DistMetric::
	~DistMetric(){
		obst_map_.data.clear();
		ptsCell.clear();
	}

	void
	DistMetric::
	initObstacle(){
		width_ = obst_map_.info.width;
		height_ = obst_map_.info.height;

		obsMap.resize(width_*height_);
		ptsCell.resize(width_*height_);

		mapPts pts;
		pts.dist = INFINITY;
		pts.sqdist = INT_MAX;
		pts.obstX = invalidObstID;
		pts.obstY = invalidObstID;
		pts.needRaise = false;
		pts.midRroad = false;
		pts.isSurround = true;
		pts.fwProcessed = false;

		for (unsigned int i = 0; i < ptsCell.size(); i++)
			ptsCell[i] = pts;

		for (int x = 0; x < width_; x++){
			for (int y = 0; y < height_; y++){
				unsigned int map_index = y * width_ + x;
				if (obst_map_.data[map_index] == OBST_VALUE)
					obsMap[map_index] = true;
				else
					obsMap[map_index] = false;
			}
		}
		/* Find the road centre, and separate the obstacle cells into whether surround or not.
		 * Only the pts that not surrounded will be processed;
		 */
		int count = 0;
		for (int x = 0; x < width_; x++){
			for (int y = 0; y < height_; y++){
				unsigned int map_index = y * width_ +x;
				if (checkNearestEightCells(x, y) && !obsMap[map_index]){
					obsMap[map_index] = true;
					ptsCell[map_index].midRroad = true;
				}
				if (obsMap[map_index]){
					if (ptsCell[map_index].isSurround){
						ptsCell[map_index].dist = 0;
						ptsCell[map_index].sqdist = 0;
						ptsCell[map_index].obstX = x;
						ptsCell[map_index].obstY = y;
					}
					else{
						setProcessedObst(x,y);
						count++;
					}
				}
			}
		}
	}

	bool
	DistMetric::
	checkNearestEightCells(int x, int y){
		unsigned int index = y * width_ +x;
		int count = 0;
		for (int i = x - 1 ; i <= x + 1; i++){
			if (i < 0 || i > width_ -1)
				continue;
			for (int j = y - 1; j <= y + 1 ; j++){
				if (i == x && j ==y)
					continue;
				if (j < 0 || j > height_ -1)
					continue;
				unsigned int neigh_index = j * width_ + i;
				if (obst_map_.data[neigh_index] != obst_map_.data[index]){
					ptsCell[index].isSurround = false;
					if (!obsMap[neigh_index])
						count ++;
				}
			}
		}
		if (count > 0)
			return true;
		else
			return false;
	}

	void DistMetric::
	setProcessedObst(int x, int y){
		unsigned int index = y * width_ + x;
		ptsCell[index].dist = 0;
		ptsCell[index].sqdist = 0;
		ptsCell[index].obstX = x;
		ptsCell[index].obstY = y;
		ptsCell[index].fwProcessed = true;
		open.push(0,INTPOINT(x,y));
	}

	bool
	DistMetric::
	isOccupied(int x, int y, mapPts pts){
		return (pts.obstX == x && pts.obstY == y);
	}

	/* @Algorithm introduced in http://www.informatik.uni-freiburg.de/~lau/dynamicvoronoi/
	 * @Raise function is not used currently, will be further employed for efficient incremental update
	 * to deal with dynamic obstacles;
	 */
	void
	DistMetric::
	updateDistMap(){
		while (!open.empty()){
			INTPOINT coord = open.pop();
			int x = coord.x;
			int y = coord.y;

			mapPts pts = ptsCell[y * width_ + x];
			if (!pts.fwProcessed)
				continue;

			if (pts.obstX != SHRT_MAX && isOccupied(pts.obstX, pts.obstY, ptsCell[pts.obstY * width_ + pts.obstX])){
				pts.fwProcessed = false;
				for (int i = x-1; i <= x + 1; i++){
					if (i < 0 || i > width_-1 )
						continue;
					for (int j = y -1 ; j <= y +1; j++){
						if (j < 0 || j > height_ -1)
							continue;
						if (i == x && j == y)
							continue;

						mapPts pts_near = ptsCell[j * width_ + i];
						int ox = pts_near.obstX;
						int oy = pts_near.obstY;
						if (!pts_near.needRaise){
							int distX = i - pts.obstX;
							int distY = j - pts.obstY;
							int newSqDist = distX*distX + distY*distY;
							bool overwrite = (newSqDist < pts_near.sqdist);
							if (newSqDist == pts_near.sqdist){
								if (pts_near.obstX == SHRT_MAX || !isOccupied(ox, oy, ptsCell[oy * width_ + ox])){
									overwrite = true;
								}
							}
							if (overwrite && (newSqDist < Max_Dist_*Max_Dist_)){
								open.push(newSqDist, INTPOINT(i,j));
								pts_near.fwProcessed = true;
								pts_near.sqdist = newSqDist;
								pts_near.dist = sqrt((double)newSqDist);
								pts_near.obstX = pts.obstX;
								pts_near.obstY = pts.obstY;
							}
							ptsCell[j * width_ + i ] = pts_near;
						}
					}
				}
			}
			ptsCell[y*width_ + x] = pts;
		}
	}

	/**
	void
	DistMetric::
	getDistMap(nav_msgs::OccupancyGrid& obst_map_in_, Metric_Map::MetricMapMsg &dist_map_out){
		obst_map_ = obst_map_in_;
		initObstacle();
		this->updateDistMap();
		for (int x = 0 ; x < width_ ; x++){
			for (int y = 0; y < height_; y++){
				unsigned int map_index = y * width_ + x;
				if (ptsCell[map_index].dist < Max_Dist_)
					dist_map_out.metric[map_index] = ptsCell[map_index].dist*obst_map_.info.resolution;
				else
					dist_map_out.metric[map_index] = 1e6;
			}
		}
	}
	*/

	void
		DistMetric::
		getDistMap(nav_msgs::OccupancyGrid& obst_map_in_, Metric_Map::MetricMapMsg &dist_map_out){
		getDistMap(obst_map_in_,dist_map_out.metric);
	}

	void
	DistMetric::
	getDistMap(nav_msgs::OccupancyGrid& obst_map_in_, vector<double> &dist_map_out){
		obst_map_ = obst_map_in_;
		initObstacle();
		this->updateDistMap();
		for (int x = 0 ; x < width_ ; x++){
			for (int y = 0; y < height_; y++){
				unsigned int map_index = y * width_ + x;
				if (ptsCell[map_index].dist < Max_Dist_)
					dist_map_out[map_index] = ptsCell[map_index].dist*obst_map_.info.resolution;
				else
					dist_map_out[map_index] = 1e6;
			}
		}
	}
}

