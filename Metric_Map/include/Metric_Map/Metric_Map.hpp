/*
 * Metric_Map.hpp
 *
 *  Created on: 27 Jul, 2014
 *      Author: liuwlz
 */

#ifndef METRIC_MAP_HPP_
#define METRIC_MAP_HPP_

#include <string>

#include <ros/ros.h>
#include <ros/console.h>

#include <sensor_msgs/PointCloud.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>
#include <nav_msgs/Path.h>

#include <Metric_Map/Scan_Fusion.hpp>
#include <Metric_Map/Dist_Map.hpp>
#include <Metric_Map/MetricMapMsg.h>

using namespace std;
using namespace boost;

namespace Perception {

	class MetricMap {

	public:
		MetricMap();
		virtual ~MetricMap();

		virtual void initObstMap();
		virtual void initPriorMap();
		virtual void updatePriorInfo();
		virtual void updateObstMap();
		virtual void updateDistMap();
		virtual void updateMetricMap(const ros::TimerEvent& e);
		virtual void addPointToObstMap(nav_msgs::OccupancyGrid& obstMap, geometry_msgs::Point32& pointIn, int obstValue);

		ros::NodeHandle nh_, pri_nh_;
		ros::Publisher obst_map_pub_, prior_pts_pub_, metric_pts_pub_, metric_map_pub_;
		ros::Timer metric_map_timer_;
		tf::TransformListener tf_;

		string global_frame_, base_frame_;
		boost::shared_ptr<ScanFusion> scan_fusion_;
		boost::shared_ptr<DistMetric> dist_metric_;

		sensor_msgs::PointCloudPtr prior_pts_;
		nav_msgs::OccupancyGridPtr obst_map_, prior_map_;
		Metric_Map::MetricMapMsgPtr metric_map_;

		int width_, height_;
		double resolution_;
		bool prior_map_init_, obst_map_init_, prior_info_init_, reference_path_got_;
	};
}  // namespace Perception

#endif /* METRIC_MAP_HPP_ */
