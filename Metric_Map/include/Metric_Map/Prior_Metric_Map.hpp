/*
 * Prior_Metric_Map.hpp
 *
 *  Created on: 30 Jul, 2014
 *      Author: liuwlz
 */

#ifndef PRIOR_METRIC_MAP_HPP_
#define PRIOR_METRIC_MAP_HPP_

#include <Metric_Map/Metric_Map.hpp>

namespace Perception {

	class PriorMetricMap:public MetricMap{
	public:
		PriorMetricMap();
		virtual ~PriorMetricMap();

		void updatePriorInfo();
		void initPriorMap();

	private:
	};

}  // namespace Perception

#endif /* PRIOR_METRIC_MAP_HPP_ */
