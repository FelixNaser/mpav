/*
 * Extend_Metric_Map.hpp
 *
 *  Created on: 30 Jul, 2014
 *      Author: liuwlz
 */

#ifndef EXTEND_METRIC_MAP_HPP_
#define EXTEND_METRIC_MAP_HPP_

#include <Metric_Map/Metric_Map.hpp>

namespace Perception {

	class ExtendMetricMap:public MetricMap {

	public:
		ExtendMetricMap();
		virtual ~ExtendMetricMap();

		void initPriorMap();
		void initReferencePath(const nav_msgs::Path& pathIn);
		void updatePriorInfo();
		void extendReferencePath();
		void densePathSegment();

		double distBetweenPose(const geometry_msgs::PoseStamped& poseA, const geometry_msgs::PoseStamped& poseB);

	private:

		ros::Subscriber reference_path_sub_;

		double extend_dist_;
		nav_msgs::Path reference_path_;
		boost::shared_ptr<DistMetric> ref_path_dist_;
		Metric_Map::MetricMapMsgPtr path_dist_map_;

		void referencePathCallBack(const nav_msgs::Path& pathIn);
	};

}  // namespace Perception

#endif /* EXTEND_METRIC_MAP_HPP_ */
